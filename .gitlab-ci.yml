stages:
  - Docker
  - Build
  - Release

image: registry.gitlab.com/${CI_PROJECT_NAMESPACE}/${CI_PROJECT_NAME}/docker:latest

variables:
  MAVEN_CLI_OPTS: "--batch-mode"
  MAVEN_OPTS: "-Dmaven.repo.local=.m2/repository"

cache:
  paths:
    - .m2/repository/
    - .mvn/

.stage_docker: &stage_docker
  stage: Docker
.stage_build: &stage_build
  stage: Build
.stage_release: &stage_release
  stage: Release

.create-docker: &create-docker
  environment:
    name: docker
  image: registry.gitlab.com/ultreiaio-infra/docker/docker:latest
  services:
    - docker:dind
  script:
    - if [ -n "${CREATE_DOCKER}" ]; then ultreiaio-docker-generate-image-11 gitlab-ci-token ${CI_REGISTRY_PASSWORD}; fi

.build: &build
  environment:
    name: build
  script:
    - if [ -n "${BUILD_FOR_RELEASE}" ]; then ultreiaio-maven-execute 'clean verify -DperformRelease -Dmaven.javadoc.skip'; fi

.deploy-snapshot: &deploy-snapshot
  environment:
    name: snapshot
  script:
    - if [ -n "${DEPLOY_SNAPSHOT}" ]; then ultreiaio-maven-execute 'clean deploy -DperformRelease -Dmaven.javadoc.skip'; fi

.publish-site: &publish-site
  environment:
    name: site
  script:
    - if [ -n "${PUBLISH_SITE}" ]; then ultreiaio-site-generate-inline; fi
    - if [ -n "${PUBLISH_SITE}" ]; then ultreiaio-git-init; fi
    - if [ -n "${PUBLISH_SITE}" ]; then ultreiaio-site-publish-only; fi

.make-release: &make-release
  environment:
    name: release
  script:
    - if [ -n "${MAKE_RELEASE}" ]; then ultreiaio-release-gitlab-init; fi
    - if [ -n "${MAKE_RELEASE}" ]; then ultreiaio-release-start; fi
    - if [ -n "${MAKE_RELEASE}" ]; then ultreiaio-release-finish; fi
    - if [ -n "${MAKE_RELEASE}" ]; then ultreiaio-milestone-create; fi

###################################################################################################
### TRIGGERS JOBS                                                                               ###
###################################################################################################

.triggers: &triggers
  only:
    - triggers

trigger-create-docker:
  <<: *stage_docker
  <<: *triggers
  <<: *create-docker

trigger-build:
  <<: *stage_build
  <<: *triggers
  <<: *build

trigger-deploy-snapshot:
  <<: *stage_release
  <<: *triggers
  <<: *deploy-snapshot

trigger-make-release:
  <<: *stage_release
  <<: *triggers
  <<: *make-release

###################################################################################################
### AUTOMATIC JOBS                                                                              ###
###################################################################################################

.automatic: &automatic
  only:
    - develop
  except:
    - triggers

build:
  <<: *stage_build
  <<: *automatic
  <<: *build

###################################################################################################
### AUTOMATIC RELEASE JOBS                                                                      ###
###################################################################################################

.automatic-release: &automatic-release
  only:
    - tags

release-create-docker:
  <<: *stage_docker
  <<: *automatic-release
  <<: *create-docker
  before_script:
    - export CREATE_DOCKER=true

release-publish-site:
  <<: *stage_build
  <<: *automatic-release
  <<: *publish-site
  before_script:
    - export PUBLISH_SITE=true

###################################################################################################
### MANUAL JOBS                                                                                 ###
###################################################################################################

.manual: &manual
  only:
    - develop
  when: manual

create-docker:
  <<: *stage_docker
  <<: *manual
  <<: *create-docker
  before_script:
    - export CREATE_DOCKER=true

deploy-snapshot:
  <<: *stage_release
  <<: *manual
  <<: *deploy-snapshot
  before_script:
    - export DEPLOY_SNAPSHOT=true

make-release:
  <<: *stage_release
  <<: *manual
  <<: *make-release
  before_script:
    - export MAKE_RELEASE=true

publish-site:
  <<: *stage_release
  <<: *manual
  <<: *publish-site
  before_script:
    - export PUBLISH_SITE=true
