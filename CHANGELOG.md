# Java Bean changelog

 * Author [Tony Chemit](mailto:dev@tchemit.fr)
 * Last generated at 2023-07-30 10:05.

## Version [0.3.3](https://gitlab.com/ultreiaio/java-bean/-/milestones/23)

**Closed at 2023-07-30.**


### Issues
  * [[bug 29]](https://gitlab.com/ultreiaio/java-bean/-/issues/29) **Fix ConcurrentModificationException on JavaBeanMonitor.reset and apply** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [0.3.2](https://gitlab.com/ultreiaio/java-bean/-/milestones/22)

**Closed at 2023-03-10.**


### Issues
  * [[enhancement 28]](https://gitlab.com/ultreiaio/java-bean/-/issues/28) **Open JavaBeanPropertyModification.oldValue API** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [0.3.1](https://gitlab.com/ultreiaio/java-bean/-/milestones/21)

**Closed at 2023-02-23.**


### Issues
  * [[enhancement 27]](https://gitlab.com/ultreiaio/java-bean/-/issues/27) **Add method JavaBeanMonitor.getModification** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [0.3.0](https://gitlab.com/ultreiaio/java-bean/-/milestones/20)

**Closed at 2023-02-21.**


### Issues
  * [[enhancement 22]](https://gitlab.com/ultreiaio/java-bean/-/issues/22) **Introduce JavaBeanMonitor and depreciate BeanMonitor API** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 23]](https://gitlab.com/ultreiaio/java-bean/-/issues/23) **JavaBeanDefinitionStore - fix generics issues** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 24]](https://gitlab.com/ultreiaio/java-bean/-/issues/24) **Remove method AbstractJavaBean.getChild** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 25]](https://gitlab.com/ultreiaio/java-bean/-/issues/25) **Remove Guava from AbstractJavaBeanDefinition** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 26]](https://gitlab.com/ultreiaio/java-bean/-/issues/26) **Remove Guava from project \o/** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [0.2.1](https://gitlab.com/ultreiaio/java-bean/-/milestones/19)

**Closed at 2022-01-23.**


### Issues
  * [[enhancement 21]](https://gitlab.com/ultreiaio/java-bean/-/issues/21) **Fix multipe compile execution (annotations are recreated which is not possible by api)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [0.2.0](https://gitlab.com/ultreiaio/java-bean/-/milestones/18)

**Closed at 2021-11-02.**


### Issues
  * [[enhancement 20]](https://gitlab.com/ultreiaio/java-bean/-/issues/20) **By default do not generate predicates, instances, streams and comparators...** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [0.1.5](https://gitlab.com/ultreiaio/java-bean/-/milestones/17)

**Closed at 2021-06-10.**


### Issues
  * [[enhancement 18]](https://gitlab.com/ultreiaio/java-bean/-/issues/18) **Migrate to java 11** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 19]](https://gitlab.com/ultreiaio/java-bean/-/issues/19) **Introduce BeanMonitor (inspired from nuiton-utils but without nuiton-utils crusade, oscour)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [0.1.4](https://gitlab.com/ultreiaio/java-bean/-/milestones/16)

**Closed at 2021-01-08.**


### Issues
  * [[bug 17]](https://gitlab.com/ultreiaio/java-bean/-/issues/17) **Bad setter generation if coming from super class with generics** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [0.1.3](https://gitlab.com/ultreiaio/java-bean/-/milestones/15)

**Closed at 2020-12-06.**


### Issues
  * [[bug 16]](https://gitlab.com/ultreiaio/java-bean/-/issues/16) **JavaBean processor does not acquire properties from interfaces** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [0.1.2](https://gitlab.com/ultreiaio/java-bean/-/milestones/14)

**Closed at 2020-10-30.**


### Issues
  * [[bug 14]](https://gitlab.com/ultreiaio/java-bean/-/issues/14) **Got same nasty errors with generic on setter methods** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[bug 15]](https://gitlab.com/ultreiaio/java-bean/-/issues/15) **Got same nasty errors with generic on loadType method** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [0.1.1](https://gitlab.com/ultreiaio/java-bean/-/milestones/13)

**Closed at 2020-03-02.**


### Issues
  * [[enhancement 13]](https://gitlab.com/ultreiaio/java-bean/-/issues/13) **Downgrade to java8, but make it usable for later jdk** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [0.1.0](https://gitlab.com/ultreiaio/java-bean/-/milestones/12)

**Closed at 2020-02-17.**


### Issues
  * [[enhancement 12]](https://gitlab.com/ultreiaio/java-bean/-/issues/12) **Migrates to java 10** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [0.0.10](https://gitlab.com/ultreiaio/java-bean/-/milestones/11)

**Closed at 2019-12-17.**


### Issues
  * [[bug 11]](https://gitlab.com/ultreiaio/java-bean/-/issues/11) **Can&#39;t generate if bean has a XXX&lt;?&gt; field** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [0.0.9](https://gitlab.com/ultreiaio/java-bean/-/milestones/10)

**Closed at 2018-11-13.**


### Issues
  * [[enhancement 9]](https://gitlab.com/ultreiaio/java-bean/-/issues/9) **Go back to java 1.8, some other library are not ready for the big step...** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [0.0.8](https://gitlab.com/ultreiaio/java-bean/-/milestones/9)

**Closed at 2018-11-13.**


### Issues
  * [[enhancement 8]](https://gitlab.com/ultreiaio/java-bean/-/issues/8) **Migrate to java 10** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [0.0.7](https://gitlab.com/ultreiaio/java-bean/-/milestones/8)

**Closed at *In progress*.**


### Issues
  * [[enhancement 7]](https://gitlab.com/ultreiaio/java-bean/-/issues/7) **Fix generation of Bean with generic inheritance** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [0.0.6](https://gitlab.com/ultreiaio/java-bean/-/milestones/7)

**Closed at *In progress*.**


### Issues
No issue.

## Version [0.0.5](https://gitlab.com/ultreiaio/java-bean/-/milestones/6)

**Closed at *In progress*.**


### Issues
  * [[bug 6]](https://gitlab.com/ultreiaio/java-bean/-/issues/6) **Fix setter in java bean definition generation when setter use a variable type** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [0.0.4](https://gitlab.com/ultreiaio/java-bean/-/milestones/5)

**Closed at *In progress*.**


### Issues
  * [[enhancement 4]](https://gitlab.com/ultreiaio/java-bean/-/issues/4) **Use java-util (Predicates and Comparators)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 5]](https://gitlab.com/ultreiaio/java-bean/-/issues/5) **Improve Query API** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [0.0.3](https://gitlab.com/ultreiaio/java-bean/-/milestones/4)

**Closed at *In progress*.**


### Issues
  * [[enhancement 2]](https://gitlab.com/ultreiaio/java-bean/-/issues/2) **Improve JavaBeanDefinition design (introduce JavaBeanPropetryDefinition)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 3]](https://gitlab.com/ultreiaio/java-bean/-/issues/3) **Introduce JavaBeanInstanceBuilder to create bean with fluent API** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [0.0.2](https://gitlab.com/ultreiaio/java-bean/-/milestones/3)

**Closed at *In progress*.**


### Issues
  * [[enhancement 1]](https://gitlab.com/ultreiaio/java-bean/-/issues/1) **Add a simple (and generated) query API on JavaBean** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [0.0.1](https://gitlab.com/ultreiaio/java-bean/-/milestones/2)

**Closed at *In progress*.**


### Issues
No issue.

