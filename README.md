# Java Bean extends by Ultreia.io

[![Maven Central status](https://img.shields.io/maven-central/v/io.ultreia.java4all/java-bean.svg)](https://search.maven.org/#search%7Cga%7C1%7Cg%3A%22io.ultreia.java4all%22%20AND%20a%3A%22java-bean%22)
[![Build Status](https://gitlab.com/ultreiaio/java-bean/badges/develop/pipeline.svg)](https://gitlab.com/ultreiaio/java-bean/pipelines)
[![The GNU Lesser General Public License, Version 3.0](https://img.shields.io/badge/license-LGPL3-grren.svg)](http://www.gnu.org/licenses/lgpl-3.0.txt)

This project offers some new API to access object as JavaBean **without any Reflection API usage** (runtime is evil).

# Abstract

With Java 8 and new functional API, a lots of new exciting things can be done now to describe objects at compile time \o/.

This means Reflection API can be easily  replaced by generated code using lambda syntax. 

Speaking on JavaBean terms, a getter is a ```java.util.function.Function```, and a setter is a ```java.util.function.BiConsumer```.

This API replace then other neutral API to access objects as a JavaBean such as common-beanutils, nuiton-utils Binder 
API, and any Runtime Reflection API based.

Using compile code instead of runtime jvm code make stuff robust and without any surprise (such as proxies) at runtime.

# First usage

Using ```io.ultreia.java4all.bean.JavaBean``` is quite simple:

  * Add ```io.ultreia.java4all.bean.JavaBean``` contract or extends ```io.ultreia.java4all.bean.AbstractJavaBean```
  * Add ```io.ultreia.java4all.bean.spi.GenerateJavaBeanDefinition``` annotation on your class
  * Compile (with annotation processing enabled!)
  * A new class was generated **```XXXJavaBeanDefinition```** (you don't need to know it)
  
That's it! You are ready to use ```JavaBean``` API on your object.

You can apply it on any type of java objects (interface or class), so there is no constraint using it, so you 
can apply it on any existing code.

# JavaBean API

```JavaBean``` API offers some methods to work on your object:

  * get a value
  * set a value
  * copy all values to another bean
  * clear all values
  * property changes support

# Instance builder API

Let's talk about how to build instance with a fluent API on your JavaBeans.

By default, for each ```JavaBean``` processed by compiler, a instance builder class is also generated that offers a nice API 
to generate instances, named **```XXXInstanceBuilder```**.

You can access it statically via
``` 
XXXJavaBeanDefinition.instance()
``` 
or from a java bean (or his definition)

``` 
javaBeanDefinition.instanceBuilder();
javaBean.instanceBuilder();
``` 
  
The ```Instance builder``` API is **100%** compile, strongly type safe.

To disable generation of ```Instance builder``` API, just configure the annotation:
```
@GenerateJavaBeanDefinition(instance = false)
```
**Important note** If you disable instance, then previous methods will throw a ```UnsupportedOperationException```.
  
# Predicate API

Let's talk about how to generate predicates on your JavaBeans.

By default, for each ```JavaBean``` processed by compiler, a class is also generated that offers a nice API 
to generate predicates, named **```XXXPredicate```**.

You can access it statically via
``` 
XXXJavaBeanDefinition.predicate()
``` 
or from a java bean (or his definition)

``` 
javaBeanDefinition.predicateBuilder();
javaBean.predicateBuilder();
``` 

**Important note** PredicateBuilder is already a predicate, so If you disable predicate, then previous methods will throw a ```UnsupportedOperationException```.
  
The ```Predicate builder``` API is **100%** compile, strongly type safe.

To disable generation of ```Predicate builder``` API, just configure the annotation:
```
@GenerateJavaBeanDefinition(predicate = false)
```
**Important note** If you disable predicate, then previous methods will throw a ```UnsupportedOperationException```.

# Comparator builder API

Let's talk about how to create comparators on your JavaBeans.

By default, for each ```JavaBean``` processed by compiler, a comparator builder class is also generated that offers a nice API 
to generate comparators, named **```XXXComparatorBuilder```**.

You can access it statically via
``` 
XXXJavaBeanDefinition.comparator()
``` 
or from a java bean (or his definition)

``` 
javaBeanDefinition.comparatorBuilder();
javaBean.comparatorBuilder();
``` 

The ```Comparator builder``` API is **100%** compile, strongly type.

To disable generation of ```Comparator builder``` API, just configure the annotation:
```
@GenerateJavaBeanDefinition(comparator = false)
```

**Important note** If you disable comparator, then previous methods will throw a ```UnsupportedOperationException```.

# Stream API

Let's talk about how to generate streams helper on your JavaBeans.

By default, for each ```JavaBean``` processed by compiler, a stream helper class is also generated that offers a nice API 
to generate predicates or comparator inside a collections, named **```XXXStream```**.

You can access it statically via
``` 
XXXJavaBeanDefinition.stream(myCollection)
``` 
  
The ```Stream``` API is **100%** compile, strongly type safe.

To disable generation of ```Stream``` API, just configure the annotation:
```
@GenerateJavaBeanDefinition(stream = false)
```
**Important note** If you disable stream, then previous methods will throw a ```UnsupportedOperationException```.

# More to come

There is a lot that can be done, this library is quite new, I have this following ideas:

  * Improve Copy API (to get a deep copy of nested JavaBean properties))
  * Introduce ToString API (again a decorator is just a ```java.util.function.Function```)
  * And more documentation please ;)

# Resources

* [Changelog and downloads](https://gitlab.com/ultreiaio/java-bean/blob/develop/CHANGELOG.md)
* [Documentation](http://ultreiaio.gitlab.io/java-bean)

# Community

* [Contact](mailto:incoming+ultreiaio/java-bean@gitlab.com)
