package io.ultreia.java4all.bean.spi;

/*-
 * #%L
 * Java Beans extends by Ultreia.io
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.bean.JavaBean;
import io.ultreia.java4all.bean.definition.JavaBeanDefinition;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Place this annotation on any {@link JavaBean} class to generate his {@link JavaBeanDefinition}.
 * <p>
 * Created by tchemit on 07/01/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.SOURCE)
public @interface GenerateJavaBeanDefinition {
    /**
     * @return array of fully qualified class names that should be register for this definition, if none is
     * specified, then use the processed java class type.
     * @see JavaBeanDefinition#types()
     */
    String[] types() default {};

    /** @return {@code true} to generate predicate builder */
    boolean predicate() default false;

    /** @return {@code true} to generate comparator builder */
    boolean comparator() default false;

    /** @return {@code true} to generate stream helper */
    boolean stream() default false;

    /** @return {@code true} to generate instance builder */
    boolean instance() default false;
}

