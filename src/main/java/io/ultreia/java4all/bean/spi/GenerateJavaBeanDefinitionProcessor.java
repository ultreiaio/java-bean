package io.ultreia.java4all.bean.spi;

/*-
 * #%L
 * Java Beans extends by Ultreia.io
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import io.ultreia.java4all.bean.AbstractJavaBeanComparatorBuilder;
import io.ultreia.java4all.bean.AbstractJavaBeanInstanceBuilder;
import io.ultreia.java4all.bean.AbstractJavaBeanPredicate;
import io.ultreia.java4all.bean.AbstractJavaBeanStream;
import io.ultreia.java4all.bean.definition.AbstractJavaBeanDefinition;
import io.ultreia.java4all.bean.definition.JavaBeanDefinition;
import io.ultreia.java4all.util.ImportManager;

import javax.annotation.Generated;
import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.Processor;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.annotation.processing.SupportedOptions;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.PackageElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.TypeParameterElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.NoType;
import javax.lang.model.type.PrimitiveType;
import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.AbstractElementVisitor8;
import javax.lang.model.util.Elements;
import javax.lang.model.util.Types;
import javax.tools.Diagnostic;
import javax.tools.FileObject;
import javax.tools.JavaFileObject;
import javax.tools.StandardLocation;
import java.beans.Introspector;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Created by tchemit on 07/01/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
@SupportedAnnotationTypes("io.ultreia.java4all.bean.spi.GenerateJavaBeanDefinition")
@AutoService(Processor.class)
@SupportedOptions({"debug"})
public class GenerateJavaBeanDefinitionProcessor extends AbstractProcessor {

    private static final String DEFINITION_JAVA_FILE = "package %1$s;\n" +
            "\n" +
            "%2$s" +
            "\n@AutoService(JavaBeanDefinition.class)" +
            "\n@Generated(value = \"%3$s\", date = \"%4$s\")" +
            "\npublic final class %5$sJavaBeanDefinition%6$s extends AbstractJavaBeanDefinition {\n\n" +
            "    @Override\n" +
            "    protected final java.util.Set<Class<?>> loadAcceptedTypes() {\n" +
            "%7$s" +
            "    }\n\n" +
            "    @Override\n" +
            "    protected final java.util.Map<String, Class<?>> loadTypes() {\n" +
            "%8$s" +
            "    }\n\n" +
            "    @SuppressWarnings(\"unchecked\")\n" +
            "    @Override\n" +
            "    protected final java.util.Map<String, Function<?, ?>> loadGetters() {\n" +
            "%9$s" +
            "    }\n\n" +
            "    @SuppressWarnings(\"unchecked\")\n" +
            "    @Override\n" +
            "    protected final java.util.Map<String, BiConsumer<?, ?>> loadSetters() {\n" +
            "%10$s" +
            "    }\n" +
            "%11$s%12$s%13$s" +
            "\n" +
            "}\n";

    private static final String PREDICATE_JAVA_FILE = "\n    public static final class %s%s extends AbstractJavaBeanPredicate<%s%s, %s%s> {\n" +
            "\n        protected %s() {\n" +
            "            super(%sJavaBeanDefinition.class);\n" +
            "        }\n" +
            "\n        protected %s(%s javaBeanDefinition) {\n" +
            "            super(javaBeanDefinition);\n" +
            "        }\n" +
            "%s" +
            "\n    }\n";

    private static final String STREAM_JAVA_FILE = "\n    public static final class %s%s extends AbstractJavaBeanStream<%s%s, %s%s> {\n" +
            "\n        protected %s(Collection<%s%s> elements) {\n" +
            "            super(%sJavaBeanDefinition.class, elements);\n" +
            "        }\n" +
            "\n        protected %s(%sJavaBeanDefinition javaBeanDefinition, Collection<%s%s> elements) {\n" +
            "            super(javaBeanDefinition, elements);\n" +
            "        }\n" +
            "%s" +
            "\n    }\n";

    private static final String COMPARATOR_JAVA_FILE = "\n    public static final class %s%s extends AbstractJavaBeanComparatorBuilder<%s%s, %s%s> {\n" +
            "\n        protected %s() {\n" +
            "            super(%sJavaBeanDefinition.class);\n" +
            "        }\n" +
            "\n        protected %s(%s javaBeanDefinition) {\n" +
            "            super(javaBeanDefinition);\n" +
            "        }\n" +
            "%s" +
            "\n    }\n";
    private static final String INSTANCE_JAVA_FILE = "\n    public static final class %s%s extends AbstractJavaBeanInstanceBuilder<%s%s, %s%s> {\n" +
            "\n        protected %s() {\n" +
            "            super(%sJavaBeanDefinition.class);\n" +
            "        }\n" +
            "\n        protected %s(%s javaBeanDefinition) {\n" +
            "            super(javaBeanDefinition);\n" +
            "        }\n" +
            "%s" +
            "\n    }\n";

    private static final List<String> EXCLUDED_PROPERTIES = Arrays.asList("ters", "class", "propertyChangeListeners", "vetoableChangeListeners");
    private static final String LOAD_ACCEPTED_TYPES = "        java.util.Set<Class<?>> result = new java.util.LinkedHashSet<>();" +
            "%s\n" +
            "        return java.util.Collections.unmodifiableSet(result);\n";
    private static final String LOAD_TYPES = "        java.util.Map<String, Class<?>> result = new java.util.LinkedHashMap<>();" +
            "%s\n" +
            "        return java.util.Collections.unmodifiableMap(result);\n";
    private static final String GETTERS = "        java.util.Map<String, Function<%s, ?>> result = new java.util.LinkedHashMap<>();" +
            "%s\n" +
            "        return (java.util.Map) java.util.Collections.unmodifiableMap(result);\n";
    private static final String SETTERS = "        java.util.Map<String, BiConsumer<%s, ?>> result = new java.util.LinkedHashMap<>();" +
            "%s\n" +
            "        return (java.util.Map) java.util.Collections.unmodifiableMap(result);\n";

    private static abstract class Detector extends AbstractElementVisitor8<Map<String, ExecutableElement>, Void> {
        final Map<String, ExecutableElement> result = new TreeMap<>();
        private final ProcessingEnvironment processingEnv;
        private final Types typeUtils;
        private final Elements elementUtils;

        private Detector(ProcessingEnvironment processingEnv) {
            this.processingEnv = processingEnv;
            this.typeUtils = processingEnv.getTypeUtils();
            this.elementUtils = processingEnv.getElementUtils();
        }

        private void logDebug(String msg) {
            if (processingEnv.getOptions().containsKey("debug")) {
                processingEnv.getMessager().printMessage(Diagnostic.Kind.NOTE, msg);
            }
        }

        @Override
        public final Map<String, ExecutableElement> visitPackage(PackageElement e, Void aVoid) {
            return null;
        }

        @Override
        public final Map<String, ExecutableElement> visitType(TypeElement e, Void aVoid) {

            boolean aClass = e.getKind() == ElementKind.CLASS;
            if (aClass) {
                TypeMirror superclass = e.getSuperclass();
                if (superclass != null) {
                    TypeElement typeElement = elementUtils.getTypeElement(typeUtils.erasure(superclass).toString());
                    if (typeElement != null) {
                        visitType(typeElement, aVoid);
                    }
                }
            }

            if (aClass || e.getKind() == ElementKind.INTERFACE) {
                List<? extends TypeMirror> interfaces = e.getInterfaces();
                for (TypeMirror anInterface : interfaces) {
                    TypeElement typeElement = elementUtils.getTypeElement(typeUtils.erasure(anInterface).toString());
                    if (typeElement != null) {
                        visitType(typeElement, aVoid);
                    }
                }
            }

            for (Element element : e.getEnclosedElements()) {
                if (element.getKind() == ElementKind.METHOD) {
                    visitExecutable((ExecutableElement) element, aVoid);
                }
            }
            return result;
        }

        @Override
        public final Map<String, ExecutableElement> visitVariable(VariableElement e, Void aVoid) {
            return null;
        }

        protected abstract String acceptExecutable(ExecutableElement e);

        @Override
        public final Map<String, ExecutableElement> visitExecutable(ExecutableElement e, Void aVoid) {
            String simpleName = e.getSimpleName().toString();
            String propertyName = acceptExecutable(e);
            if (propertyName != null) {
                logDebug(String.format("%s - Found a matching method: %s (%s)", getClass().getSimpleName(), simpleName, propertyName));
                result.put(propertyName, e);
            }
            return null;
        }

        @Override
        public final Map<String, ExecutableElement> visitTypeParameter(TypeParameterElement e, Void aVoid) {
            return null;
        }
    }

    private final static class GettersDetector extends Detector {

        private final Set<String> excludeProperties;

        private GettersDetector(ProcessingEnvironment processingEnv, Set<String> excludeProperties) {
            super(processingEnv);
            this.excludeProperties = excludeProperties;
        }

        @Override
        protected String acceptExecutable(ExecutableElement e) {
            String simpleName = e.getSimpleName().toString();
            boolean objectPrefix = simpleName.startsWith("get");
            boolean booleanPrefix = simpleName.startsWith("is");
            boolean result = (objectPrefix || booleanPrefix)
                    && !e.getModifiers().contains(Modifier.STATIC)
                    && e.getModifiers().contains(Modifier.PUBLIC)
                    && e.getParameters().isEmpty()
                    && !(e.getReturnType() instanceof NoType);
            if (result) {
                String propertyName = Introspector.decapitalize(booleanPrefix ? simpleName.substring(2) : simpleName.substring(3));
                return excludeProperties.contains(propertyName) ? null : propertyName;
            }
            return null;
        }

    }

    private final static class SettersDetector extends Detector {

        private final Set<String> excludeProperties;

        private SettersDetector(ProcessingEnvironment processingEnv, Set<String> excludeProperties) {
            super(processingEnv);
            this.excludeProperties = excludeProperties;
        }

        @Override
        protected String acceptExecutable(ExecutableElement e) {
            String simpleName = e.getSimpleName().toString();
            boolean result = simpleName.startsWith("set")
                    && !e.getModifiers().contains(Modifier.STATIC)
                    && e.getModifiers().contains(Modifier.PUBLIC)
                    && e.getParameters().size() == 1
                    && e.getReturnType() instanceof NoType;
            if (result) {
                String propertyName = Introspector.decapitalize(simpleName.substring(3));
                return excludeProperties.contains(propertyName) ? null : propertyName;
            }
            return null;
        }

    }

    @Override
    public SourceVersion getSupportedSourceVersion() {
        return SourceVersion.latest();
    }

    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
        boolean used = false;
        for (TypeElement annotation : annotations) {

            Set<? extends Element> annotatedElements = roundEnv.getElementsAnnotatedWith(annotation);
            for (Element annotatedElement : annotatedElements) {
                TypeElement classElement = (TypeElement) annotatedElement;

                String fullyQualifiedName = classElement.getQualifiedName().toString();
                String packageName = processingEnv.getElementUtils().getPackageOf(classElement).toString();

                String fullClassName = fullyQualifiedName.substring(packageName.length() + 1);
                String className = fullClassName;
                int i = className.indexOf(".");
                if (i > -1) {
                    className = className.substring(i + 1);
                }
                String generatedClassName = packageName + "." + className + "JavaBeanDefinition";
                try {
                    FileObject resource = processingEnv.getFiler().getResource(StandardLocation.SOURCE_OUTPUT, packageName, className + "JavaBeanDefinition.java");
                    Path javaFile = Path.of(resource.toUri().toURL().getFile());
                    if (Files.exists(javaFile)) {
                        // Already done
                        logWarning(String.format("Skip already processed class: %s", generatedClassName));
                        continue;
                    }
                } catch (IOException e) {
                    // file not found, can safely execute it
                }

                logDebug(String.format("Detect JavaBean: %s", classElement));

                Map<String, String> formalMap = computeFormalParameters(classElement);
                List<? extends TypeParameterElement> typeParameters = classElement.getTypeParameters();

                Set<String> excludeProperties = new TreeSet<>(EXCLUDED_PROPERTIES);
                Map<String, ExecutableElement> getters = classElement.accept(new GettersDetector(processingEnv, excludeProperties), null);
                Map<String, ExecutableElement> setters = classElement.accept(new SettersDetector(processingEnv, excludeProperties), null);

                List<String> formal = new LinkedList<>();
                String classNameWithFormal;
                String classNameWithRaw;
                if (typeParameters.isEmpty()) {
                    classNameWithFormal = "";
                    classNameWithRaw = "";
                } else {

                    List<String> raw = new LinkedList<>();
                    for (TypeParameterElement typeParameter : typeParameters) {
                        String simpleName = typeParameter.getSimpleName().toString();
                        String type = typeParameter.getBounds().get(0).toString();
                        formal.add(simpleName);
                        raw.add(simpleName + " extends " + type);
                    }
                    classNameWithFormal = String.format("<%s>", String.join(", ", formal));
                    classNameWithRaw = String.format("<%s>", String.join(", ", raw));
                }
                GenerateJavaBeanDefinition realAnnotation = classElement.getAnnotation(GenerateJavaBeanDefinition.class);
                try {
                    generateDefinitionFile(realAnnotation, packageName, generatedClassName, fullClassName, className, classNameWithFormal, classNameWithRaw, getters, setters, formal, formalMap);
                } catch (IOException e) {
                    throw new RuntimeException("Can't generate JavaBean definition file for: " + classElement, e);
                }
                used = true;
            }
        }
        return used;
    }

    private void generateDefinitionFile(GenerateJavaBeanDefinition realAnnotation, String packageName, String generatedClassName, String fullClassName, String className, String classNameWithFormal, String classNameWithRaw, Map<String, ExecutableElement> getters, Map<String, ExecutableElement> setters, List<String> formal, Map<String, String> formalMap) throws IOException {

        String[] classes = realAnnotation.types();
        boolean predicate = realAnnotation.predicate();
        boolean comparator = realAnnotation.comparator();
        boolean stream = realAnnotation.stream();
        boolean instance = realAnnotation.instance();

        Set<String> acceptedTypesBuilder = new LinkedHashSet<>();

        if (classes.length == 0) {
            acceptedTypesBuilder.add(fullClassName);
        } else {
            Collections.addAll(acceptedTypesBuilder, classes);
        }
        String acceptedTypesField = String.format(LOAD_ACCEPTED_TYPES,
                                                  acceptedTypesBuilder.stream().map(e -> "\n        result.add(" + e + ".class);").collect(Collectors.joining()));
        Set<String> typesBuilder = new LinkedHashSet<>();

        Set<String> properties = new LinkedHashSet<>();
        Set<String> gettersBuilder = new LinkedHashSet<>();
        for (Map.Entry<String, ExecutableElement> entry : getters.entrySet()) {
            String propertyName = entry.getKey();
            ExecutableElement methodElement = entry.getValue();
            String methodName = methodElement.getSimpleName().toString();
            String methodType = processingEnv.getTypeUtils().erasure(methodElement.getReturnType()).toString();
            gettersBuilder.add(String.format("\"%s\", %s::%s", propertyName, fullClassName, methodName));
            if (properties.add(propertyName)) {
                typesBuilder.add(String.format("\"%s\", %s", propertyName, methodType));
            }
        }

        Set<String> settersBuilder = new LinkedHashSet<>();
        for (Map.Entry<String, ExecutableElement> entry : setters.entrySet()) {
            String propertyName = entry.getKey();
            ExecutableElement methodElement = entry.getValue();
            String methodName = methodElement.getSimpleName().toString();
            Element variableElement = methodElement.getParameters().get(0);
            String methodType = computeSetterMethodParameterType(className, formal, formalMap, methodElement, variableElement);
            settersBuilder.add(String.format("\"%s\", (e, v) -> e.%s((%s) v)", propertyName, methodName, methodType));
            if (properties.add(propertyName)) {
                if (methodType.contains("<")) {
                    methodType = methodType.substring(0, methodType.indexOf('<'));
                }
                typesBuilder.add(String.format("\"%s\", %s", propertyName, methodType));
            }
        }
        String typesField = String.format(LOAD_TYPES,
                                          typesBuilder.stream().map(e -> "\n        result.put(" + e + ".class);").collect(Collectors.joining()));
        String gettersField = String.format(GETTERS,
                                            fullClassName + classNameWithFormal,
                                            gettersBuilder.stream().map(e -> "\n        result.put(" + e + ");").collect(Collectors.joining()));

        String settersField = String.format(SETTERS,
                                            fullClassName + classNameWithFormal,
                                            settersBuilder.stream().map(e -> "\n        result.put(" + e + ");").collect(Collectors.joining()));

        ImportManager importManager = new ImportManager(packageName);
        importManager.addImport(AutoService.class);
        importManager.addImport(Generated.class);
        importManager.addImport(JavaBeanDefinition.class);
        importManager.addImport(AbstractJavaBeanDefinition.class);
        importManager.addImport(Function.class);
        importManager.addImport(BiConsumer.class);
        if (!className.equals(fullClassName)) {
            importManager.addImport(packageName + "." + fullClassName);
        }
        boolean empty = classNameWithFormal.isEmpty();
        String addFormal = empty ? "" : "<>";
        StringBuilder extraStaticsMethods = new StringBuilder();
        StringBuilder extraMethods = new StringBuilder();
        StringBuilder extraClasses = new StringBuilder();
        if (predicate) {
            importManager.addImport(AbstractJavaBeanPredicate.class);

            extraClasses.append(generatePredicateBuilderFile(className + "Predicate", className, classNameWithFormal, classNameWithRaw, getters));

            extraMethods.append(String.format("\n    @Override\n" +
                                                      "    public %sPredicate predicateBuilder() {\n" +
                                                      "        return new %sPredicate(this);\n" +
                                                      "    }\n", className, className));
            extraStaticsMethods.append(String.format("\n    public static %s%sPredicate%s predicate() {\n" +
                                                             "        return new %sPredicate%s();\n" +
                                                             "    }\n", empty ? "" : classNameWithRaw + " ", className, classNameWithFormal, className, addFormal));

        }
        if (stream) {
            importManager.addImport(AbstractJavaBeanStream.class);
            importManager.addImport(Collection.class);

            extraClasses.append(generateStreamFile(className + "Stream", className, classNameWithFormal, classNameWithRaw, getters));

            extraStaticsMethods.append(String.format("\n    public static %s%sStream%s stream(Collection<%s%s> elements) {\n" +
                                                             "        return new %sStream%s(elements);\n" +
                                                             "    }\n", empty ? "" : classNameWithRaw + " ", className, classNameWithFormal, className, classNameWithFormal, className, addFormal));

        }
        if (comparator) {
            importManager.addImport(AbstractJavaBeanComparatorBuilder.class);

            extraClasses.append(generateComparatorBuilderFile(className + "ComparatorBuilder", className, classNameWithFormal, classNameWithRaw, getters));

            extraMethods.append(String.format("\n    @Override\n" +
                                                      "    public %sComparatorBuilder comparatorBuilder() {\n" +
                                                      "        return new %sComparatorBuilder(this);\n" +
                                                      "    }\n", className, className));
            extraStaticsMethods.append(String.format("\n    public static %s%sComparatorBuilder%s comparator() {\n" +
                                                             "        return new %sComparatorBuilder%s();\n" +
                                                             "    }\n", empty ? "" : classNameWithRaw + " ", className, classNameWithFormal, className, addFormal));
        }
        if (instance) {
            importManager.addImport(AbstractJavaBeanInstanceBuilder.class);
            extraClasses.append(generateInstanceBuilderFile(className + "InstanceBuilder", className, classNameWithFormal, classNameWithRaw, setters));

            extraMethods.append(String.format("\n    @Override\n" +
                                                      "    public %sInstanceBuilder instanceBuilder() {\n" +
                                                      "        return new %sInstanceBuilder(this);\n" +
                                                      "    }\n", className, className));
            extraStaticsMethods.append(String.format("\n    public static %s%sInstanceBuilder%s instance() {\n" +
                                                             "        return new %sInstanceBuilder%s();\n" +
                                                             "    }\n", empty ? "" : classNameWithRaw + " ", className, classNameWithFormal, className, addFormal));
        }

        String imports = importManager.getImportsSection("\n");
        String content = String.format(DEFINITION_JAVA_FILE,
                /*  1 */    packageName,
                /*  2 */    imports,
                /*  3 */    getClass().getName(),
                /*  4 */    new Date(),
                /*  5 */    className,
                /*  6 */    classNameWithRaw,
                /*  7 */    acceptedTypesField,
                /*  8 */    typesField,
                /*  9 */    gettersField,
                /* 10 */    settersField,
                /* 11 */    extraStaticsMethods,
                /* 12 */    extraMethods,
                /* 13 */    extraClasses);
        generate(generatedClassName, content);
    }

    private Map<String, String> computeFormalParameters(TypeElement classElement) {
        Map<String, String> formalMap = new TreeMap<>();
        TypeMirror superclass = classElement.getSuperclass();
        while (superclass != null) {
            TypeElement superclassElement = (TypeElement) processingEnv.getTypeUtils().asElement(superclass);
            if (superclassElement == null) {
                break;
            }
            List<? extends TypeParameterElement> superClassTypeParameters = superclassElement.getTypeParameters();
            for (TypeParameterElement typeParameter : superClassTypeParameters) {
                String simpleName = typeParameter.getSimpleName().toString();
                if (formalMap.containsKey(simpleName)) {
                    continue;
                }
                TypeMirror typeMirror = processingEnv.getTypeUtils().asMemberOf((DeclaredType) classElement.asType(), typeParameter);
                formalMap.put(simpleName, typeMirror.toString());
                logDebug(String.format("Add formal parameter: %s → %s", simpleName, typeMirror));
            }
            superclass = superclassElement.getSuperclass();
        }
        return formalMap;
    }

    private String computeSetterMethodParameterType(String className, List<String> formal, Map<String, String> formalMap, ExecutableElement methodElement, Element variableElement) {
        if (!methodElement.getTypeParameters().isEmpty() && variableElement.asType().getKind() == TypeKind.TYPEVAR && !formal.contains(variableElement.asType().toString())) {
            return processingEnv.getTypeUtils().erasure(variableElement.asType()).toString();
        }
        String methodType = variableElement.asType().toString();
        if (formalMap.containsKey(methodType)) {
            return formalMap.get(methodType);
        }
        if (!methodType.contains("<")) {
            return methodType;
        }
        if (methodType.matches(".+<\\s*\\?\\s*>\\s*$")) {
            return methodType;
        }
        DeclaredType capture = (DeclaredType) processingEnv.getTypeUtils().capture(variableElement.asType());
        List<String> formalValues = new LinkedList<>();
        boolean use = false;
        for (TypeMirror typeArgument : capture.getTypeArguments()) {
            String simpleName = typeArgument.toString();
            String rawValue = formalMap.get(simpleName);
            if (rawValue != null) {
                formalValues.add(rawValue);
            } else {
                String toString = typeArgument.toString();
                if (toString.startsWith("capture")) {
                    use = false;
                    break;
                }
                formalValues.add(toString);
            }
            use = true;
        }
        if (use) {
            methodType = String.format("%s< %s >", processingEnv.getTypeUtils().erasure(variableElement.asType()).toString(), String.join(", ", formalValues));
        }
        logDebug(String.format("Class %s - Method Parameterized type: %s", className, methodType));

        return methodType;
    }

    private String generatePredicateBuilderFile(String predicateSimpleClassName, String className, String classNameWithFormal, String classNameWithRaw, Map<String, ExecutableElement> getters) {

        List<String> methods = new LinkedList<>();
        Types typeUtils = processingEnv.getTypeUtils();
        TypeMirror t2 = typeUtils.erasure(processingEnv.getElementUtils().getTypeElement(Comparable.class.getName()).asType());

        for (Map.Entry<String, ExecutableElement> entry : getters.entrySet()) {
            ExecutableElement value = entry.getValue();
            TypeMirror returnType = value.getReturnType();
            boolean comparable = false;
            for (TypeMirror typeMirror : typeUtils.directSupertypes(returnType)) {
                TypeMirror erasure = typeUtils.erasure(typeMirror);
                if (t2.equals(erasure)) {
                    comparable = true;
                    break;
                }
            }
            String propertyName = entry.getKey();
            String returnTypeName;
            boolean primitiveType = returnType instanceof PrimitiveType;
            if (primitiveType) {
                returnTypeName = typeUtils.boxedClass((PrimitiveType) returnType).toString();
                comparable = true;
            } else {
                returnTypeName = typeUtils.erasure(returnType).toString();
            }
            boolean booleanType = returnType.toString().toLowerCase().endsWith("boolean");
            String methodName = value.getSimpleName().toString();
            if (methodName.startsWith("is")) {
                methodName = "where" + methodName.substring(2);
            } else {
                methodName = "where" + methodName.substring(3);
            }
            if (booleanType) {
                methods.add(String.format("\n" +
                                                  "        public %s<%s, %s, ?> %s() {\n" +
                                                  "            return %s(\"%s\");\n" +
                                                  "        }"
                        , primitiveType ? "PrimitiveBooleanQuery" : "ObjectBooleanQuery"
                        , className + classNameWithFormal
                        , predicateSimpleClassName + classNameWithFormal
                        , methodName
                        , primitiveType ? "wherePrimitiveBoolean" : "whereBoolean"
                        , propertyName
                ));
            } else if (comparable) {
                if (returnTypeName.contains(".String")) {
                    methods.add(String.format("\n" +
                                                      "        public StringQuery<%s, %s, ?> %s() {\n" +
                                                      "            return whereString(\"%s\");\n" +
                                                      "        }"
                            , className + classNameWithFormal
                            , predicateSimpleClassName + classNameWithFormal
                            , methodName
                            , propertyName));
                } else {
                    methods.add(String.format("\n" +
                                                      "        public %s<%s, %s, %s, ?> %s() {\n" +
                                                      "            return %s(\"%s\");\n" +
                                                      "        }"
                            , primitiveType ? "PrimitiveObjectQuery" : "ComparableQuery"
                            , className + classNameWithFormal
                            , returnTypeName
                            , predicateSimpleClassName + classNameWithFormal
                            , methodName
                            , primitiveType ? "wherePrimitive" : "whereComparable"
                            , propertyName));
                }
            } else {
                methods.add(String.format("\n" +
                                                  "        public ObjectQuery<%s, %s, %s, ?> %s() {\n" +
                                                  "            return where(\"%s\");\n" +
                                                  "        }"
                        , className + classNameWithFormal
                        , returnTypeName
                        , predicateSimpleClassName + classNameWithFormal
                        , methodName
                        , propertyName
                ));
            }
        }
        return String.format(PREDICATE_JAVA_FILE,

                             predicateSimpleClassName,
                             classNameWithRaw,
                             className,
                             classNameWithFormal,
                             predicateSimpleClassName,
                             classNameWithFormal,

                             predicateSimpleClassName,
                             className,
                             predicateSimpleClassName,
                             className + "JavaBeanDefinition",
                             String.join("\n", methods));
    }

    private String generateStreamFile(String predicateSimpleClassName, String className, String classNameWithFormal, String classNameWithRaw, Map<String, ExecutableElement> getters) {

        List<String> methods = new LinkedList<>();
        Types typeUtils = processingEnv.getTypeUtils();
        TypeMirror t2 = typeUtils.erasure(processingEnv.getElementUtils().getTypeElement(Comparable.class.getName()).asType());

        for (Map.Entry<String, ExecutableElement> entry : getters.entrySet()) {
            ExecutableElement value = entry.getValue();
            TypeMirror returnType = value.getReturnType();
            boolean comparable = false;
            for (TypeMirror typeMirror : typeUtils.directSupertypes(returnType)) {
                TypeMirror erasure = typeUtils.erasure(typeMirror);
                if (t2.equals(erasure)) {
                    comparable = true;
                    break;
                }
            }
            String propertyName = entry.getKey();
            String returnTypeName;
            boolean primitiveType = returnType instanceof PrimitiveType;
            if (primitiveType) {
                returnTypeName = typeUtils.boxedClass((PrimitiveType) returnType).toString();
                comparable = true;
            } else {
                returnTypeName = typeUtils.erasure(returnType).toString();
            }
            boolean booleanType = returnType.toString().toLowerCase().endsWith("boolean");
            String methodName = value.getSimpleName().toString();
            if (methodName.startsWith("is")) {
                methodName = "where" + methodName.substring(2);
            } else {
                methodName = "where" + methodName.substring(3);
            }
            if (booleanType) {
                methods.add(String.format("\n" +
                                                  "        public %s<%s, %s, ?> %s() {\n" +
                                                  "            return %s(\"%s\");\n" +
                                                  "        }"
                        , primitiveType ? "StreamPrimitiveBooleanQuery" : "StreamObjectBooleanQuery"
                        , className + classNameWithFormal
                        , predicateSimpleClassName + classNameWithFormal
                        , methodName
                        , primitiveType ? "wherePrimitiveBoolean" : "whereBoolean"
                        , propertyName
                ));
            } else if (comparable) {
                if (returnTypeName.contains(".String")) {
                    methods.add(String.format("\n" +
                                                      "        public StreamStringQuery<%s, %s, ?> %s() {\n" +
                                                      "            return whereString(\"%s\");\n" +
                                                      "        }"
                            , className + classNameWithFormal
                            , predicateSimpleClassName + classNameWithFormal
                            , methodName
                            , propertyName));
                } else {
                    methods.add(String.format("\n" +
                                                      "        public %s<%s, %s, %s, ?> %s() {\n" +
                                                      "            return %s(\"%s\");\n" +
                                                      "        }"
                            , primitiveType ? "StreamPrimitiveObjectQuery" : "StreamComparableQuery"
                            , className + classNameWithFormal
                            , returnTypeName
                            , predicateSimpleClassName + classNameWithFormal
                            , methodName
                            , primitiveType ? "wherePrimitive" : "whereComparable"
                            , propertyName));
                }
            } else {
                methods.add(String.format("\n" +
                                                  "        public StreamObjectQuery<%s, %s, %s, ?> %s() {\n" +
                                                  "            return where(\"%s\");\n" +
                                                  "        }"
                        , className + classNameWithFormal
                        , returnTypeName
                        , predicateSimpleClassName + classNameWithFormal
                        , methodName
                        , propertyName
                ));
            }
        }
        return String.format(STREAM_JAVA_FILE,

                             predicateSimpleClassName,
                             classNameWithRaw,
                             className,
                             classNameWithFormal,
                             predicateSimpleClassName,
                             classNameWithFormal,

                             predicateSimpleClassName,
                             className,
                             classNameWithFormal,
                             className,

                             predicateSimpleClassName,
                             className,
                             className,
                             classNameWithFormal,
                             String.join("\n", methods));
    }

    private String generateComparatorBuilderFile(String comparatorSimpleClassName, String className, String classNameWithFormal, String classNameWithRaw, Map<String, ExecutableElement> getters) {

        List<String> methods = new LinkedList<>();
        Types typeUtils = processingEnv.getTypeUtils();
        TypeMirror t2 = typeUtils.erasure(processingEnv.getElementUtils().getTypeElement(Comparable.class.getName()).asType());

        for (Map.Entry<String, ExecutableElement> entry : getters.entrySet()) {
            ExecutableElement value = entry.getValue();
            TypeMirror returnType = value.getReturnType();
            boolean comparable = false;
            for (TypeMirror typeMirror : typeUtils.directSupertypes(returnType)) {
                TypeMirror erasure = typeUtils.erasure(typeMirror);
                if (t2.equals(erasure)) {
                    comparable = true;
                    break;
                }
            }
            String propertyName = entry.getKey();
            String returnTypeName;
            boolean primitiveType = returnType instanceof PrimitiveType;
            if (primitiveType) {
                returnTypeName = typeUtils.boxedClass((PrimitiveType) returnType).toString();
                comparable = true;
            } else {
                returnTypeName = typeUtils.erasure(returnType).toString();
            }
            if (comparable) {

                String methodName = value.getSimpleName().toString();
                methodName = methodName.startsWith("is") ? "where" + methodName.substring(2) : "where" + methodName.substring(3);

                methods.add(String.format("\n" +
                                                  "        public Query<%s, %s, %s> %s() {\n" +
                                                  "            return on(\"%s\");\n" +
                                                  "        }"
                        , className + classNameWithFormal
                        , returnTypeName
                        , comparatorSimpleClassName + classNameWithFormal
                        , methodName
                        , propertyName));
            }

        }
        return String.format(COMPARATOR_JAVA_FILE,

                             comparatorSimpleClassName,
                             classNameWithRaw,
                             className,
                             classNameWithFormal,
                             comparatorSimpleClassName,
                             classNameWithFormal,

                             comparatorSimpleClassName,
                             className,
                             comparatorSimpleClassName,
                             className + "JavaBeanDefinition",
                             String.join("\n", methods));
    }

    private String generateInstanceBuilderFile(String instanceSimpleClassName, String className, String classNameWithFormal, String classNameWithRaw, Map<String, ExecutableElement> setters) {

        List<String> methods = new LinkedList<>();
        Types typeUtils = processingEnv.getTypeUtils();

        for (Map.Entry<String, ExecutableElement> entry : setters.entrySet()) {
            ExecutableElement value = entry.getValue();
            TypeMirror methodType = value.getParameters().get(0).asType();
            String propertyName = entry.getKey();
            String returnTypeName = methodType instanceof PrimitiveType
                    ? typeUtils.boxedClass((PrimitiveType) methodType).toString()
                    : typeUtils.erasure(methodType).toString();
            String methodName = Introspector.decapitalize(value.getSimpleName().toString().substring(3));
            methods.add(String.format("\n" +
                                              "        public %s %s(%s value) {\n" +
                                              "            return set(\"%s\", value);\n" +
                                              "        }"
                    , instanceSimpleClassName + classNameWithFormal, methodName, returnTypeName
                    , propertyName));
        }
        return String.format(INSTANCE_JAVA_FILE,
                             instanceSimpleClassName,
                             classNameWithRaw,
                             className,
                             classNameWithFormal,
                             instanceSimpleClassName,
                             classNameWithFormal,

                             instanceSimpleClassName,
                             className,
                             instanceSimpleClassName,
                             className + "JavaBeanDefinition",
                             String.join("\n", methods));
    }

    private void generate(String generatedClassName, String content) throws IOException {
        JavaFileObject builderFile = processingEnv.getFiler().createSourceFile(generatedClassName);
        try (PrintWriter out = new PrintWriter(builderFile.openWriter())) {
            out.print(content);
        }
    }

    private void logDebug(String msg) {
        if (processingEnv.getOptions().containsKey("debug")) {
            processingEnv.getMessager().printMessage(Diagnostic.Kind.NOTE, msg);
        }
    }

    private void logWarning(String msg) {
//        if (processingEnv.getOptions().containsKey("debug")) {
        processingEnv.getMessager().printMessage(Diagnostic.Kind.WARNING, msg);
//        }
    }
}

