package io.ultreia.java4all.bean;

/*-
 * #%L
 * Java Beans extends by Ultreia.io
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.util.Predicates;

import java.util.Collection;
import java.util.Objects;
import java.util.function.Function;
import java.util.function.Predicate;

/**
 * Created by tchemit on 11/01/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public interface JavaBeanPredicate<O extends JavaBean, P extends JavaBeanPredicate<O, P>> extends Predicate<O> {

    void addPredicate(Predicate<O> predicate);

    Predicate<O> predicate();

    interface Query<O extends JavaBean, V, P extends JavaBeanPredicate<O, P>, Q extends Query<O, V, P, Q>> {

        P parent();

        V getter(O element);

        default P isEquals(V propertyValue) {
            return addPredicate(Predicates.Equals(propertyValue));
        }

        default P isNotEquals(V propertyValue) {
            return addPredicate(Predicates.NotEquals(propertyValue));
        }

        default P isIn(Collection<V> propertyValue) {
            return addPredicate(Predicates.In(propertyValue));
        }

        default P isNotIn(Collection<V> propertyValue) {
            return addPredicate(Predicates.NotIn(propertyValue));
        }

        default Q isEqualsAnd(V propertyValue) {
            isEquals(propertyValue);
            return q();
        }

        default Q isInAnd(Collection<V> propertyValue) {
            addPredicate(Predicates.In(propertyValue));
            return q();
        }

        default Q isNotInAnd(Collection<V> propertyValue) {
            addPredicate(Predicates.NotIn(propertyValue));
            return q();
        }

        default Q isNotEqualsAnd(V propertyValue) {
            isNotEquals(propertyValue);
            return q();
        }

        default P addPredicate(Predicate<V> predicate) {
            Objects.requireNonNull(predicate);
            P parent = parent();
            parent.addPredicate(d -> {
                V v = getter(d);
                return v != null && predicate.test(v);
            });
            return parent;
        }

        Q q();
    }

    interface BooleanQuery<O extends JavaBean, P extends JavaBeanPredicate<O, P>, Q extends ComparableQuery<O, Boolean, P, Q>> extends ComparableQuery<O, Boolean, P, Q> {
        default P isTrue() {
            return isEquals(true);
        }

        default P isFalse() {
            return isEquals(false);
        }

        default Q isTrueAnd() {
            isTrue();
            return q();
        }

        default Q isFalseAnd() {
            isFalse();
            return q();
        }
    }

    interface ComparableQuery<O extends JavaBean, V extends Comparable<V>, P extends JavaBeanPredicate<O, P>, Q extends ComparableQuery<O, V, P, Q>> extends Query<O, V, P, Q> {

        default P isBefore(V propertyValue) {
            return addPredicate(Predicates.Before(propertyValue));
        }

        default P isBeforeOrEquals(V propertyValue) {
            return addPredicate(Predicates.BeforeOrEquals(propertyValue));
        }

        default P isAfter(V propertyValue) {
            return addPredicate(Predicates.AfterOrEquals(propertyValue));
        }

        default P isAfterOrEquals(V propertyValue) {
            return addPredicate(Predicates.AfterOrEquals(propertyValue));
        }

        default P isBetween(V min, V max) {
            return addPredicate(Predicates.Between(min, max));
        }

        default P isBetweenOrEquals(V min, V max) {
            return addPredicate(Predicates.BetweenOrEquals(min, max));
        }

        default Q isBeforeAnd(V propertyValue) {
            isBefore(propertyValue);
            return q();
        }

        default Q isBeforeOrEqualsAnd(V propertyValue) {
            isBeforeOrEquals(propertyValue);
            return q();
        }

        default Q isAfterAnd(V propertyValue) {
            isAfter(propertyValue);
            return q();
        }

        default Q isAfterOrEqualsAnd(V propertyValue) {
            isAfterOrEquals(propertyValue);
            return q();
        }

        default Q isBetweenAnd(V min, V max) {
            isBetweenAnd(min, max);
            return q();
        }

        default Q isBetweenOrEqualsAnd(V min, V max) {
            isBetweenOrEqualsAnd(min, max);
            return q();
        }
    }

    abstract class QuerySupport<O extends JavaBean, V, P extends JavaBeanPredicate<O, P>, Q extends QuerySupport<O, V, P, Q>> implements Query<O, V, P, Q> {
        private final P parent;
        private final Function<O, V> getter;

        protected QuerySupport(P parent, Function<O, V> getter) {
            this.parent = parent;
            this.getter = getter;
        }

        @Override
        public final P parent() {
            return parent;
        }

        @Override
        public final V getter(O element) {
            return element == null ? null : getter.apply(element);
        }

        @SuppressWarnings("unchecked")
        public final Q q() {
            return (Q) this;
        }
    }

    class ObjectQuery<O extends JavaBean, V, P extends JavaBeanPredicate<O, P>, Q extends ObjectQuery<O, V, P, Q>> extends QuerySupport<O, V, P, Q> {

        public ObjectQuery(P parent, Function<O, V> getter) {
            super(parent, getter);
        }

        public final P isNull() {
            P parent = parent();
            parent.addPredicate(d -> {
                V v = getter(d);
                return v == null;
            });
            return parent;
        }

        public final P isNotNull() {
            P parent = parent();
            parent.addPredicate(d -> {
                V v = getter(d);
                return v != null;
            });
            return parent;
        }

        public final Q isNullAnd() {
            isNull();
            return q();
        }

        public final Q isNotNullAnd() {
            isNotNull();
            return q();
        }

    }

    class PrimitiveObjectQuery<O extends JavaBean, V extends Comparable<V>, P extends JavaBeanPredicate<O, P>, Q extends PrimitiveObjectQuery<O, V, P, Q>> extends QuerySupport<O, V, P, Q> implements ComparableQuery<O, V, P, Q> {
        public PrimitiveObjectQuery(P parent, Function<O, V> getter) {
            super(parent, getter);
        }
    }

    class PrimitiveBooleanQuery<O extends JavaBean, P extends JavaBeanPredicate<O, P>, Q extends PrimitiveBooleanQuery<O, P, Q>> extends PrimitiveObjectQuery<O, Boolean, P, Q> implements BooleanQuery<O, P, Q> {
        public PrimitiveBooleanQuery(P parent, Function<O, Boolean> getter) {
            super(parent, getter);
        }
    }

    class ObjectBooleanQuery<O extends JavaBean, P extends JavaBeanPredicate<O, P>, Q extends ObjectBooleanQuery<O, P, Q>> extends SimpleComparableQuery<O, Boolean, P, Q> implements BooleanQuery<O, P, Q> {
        public ObjectBooleanQuery(P parent, Function<O, Boolean> getter) {
            super(parent, getter);
        }
    }

    class SimpleComparableQuery<O extends JavaBean, V extends Comparable<V>, P extends JavaBeanPredicate<O, P>, Q extends SimpleComparableQuery<O, V, P, Q>> extends ObjectQuery<O, V, P, Q> implements ComparableQuery<O, V, P, Q> {
        public SimpleComparableQuery(P parent, Function<O, V> getter) {
            super(parent, getter);
        }
    }

    class StringQuery<O extends JavaBean, P extends JavaBeanPredicate<O, P>, Q extends StringQuery<O, P, Q>> extends SimpleComparableQuery<O, String, P, Q> {

        public StringQuery(P parent, Function<O, String> getter) {
            super(parent, getter);
        }

        public final P contains(String propertyValue) {
            return addPredicate(Predicates.StringContains(propertyValue));
        }

        public final P notContains(String propertyValue) {
            return addPredicate(Predicates.NotStringContains(propertyValue));
        }

        public final P matches(String propertyValue) {
            return addPredicate(Predicates.StringMatches(propertyValue));
        }

        public final P notMatches(String propertyValue) {
            return addPredicate(Predicates.NotStringMatches(propertyValue));
        }

        public final Q containsAnd(String propertyValue) {
            addPredicate(Predicates.StringContains(propertyValue));
            return q();
        }

        public final Q notContainsAnd(String propertyValue) {
            addPredicate(Predicates.NotStringContains(propertyValue));
            return q();
        }

        public final Q matchesAnd(String propertyValue) {
            addPredicate(Predicates.StringMatches(propertyValue));
            return q();
        }

        public final Q notMatchesAnd(String propertyValue) {
            addPredicate(Predicates.NotStringMatches(propertyValue));
            return q();
        }

    }

}
