package io.ultreia.java4all.bean;

/*-
 * #%L
 * Java Beans extends by Ultreia.io
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.util.Comparator;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;

/**
 * Created by tchemit on 11/01/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public interface JavaBeanComparatorBuilder<O extends JavaBean> {

    void addComparator(Comparator<O> predicate);

    Optional<Comparator<O>> build();

    class Query<O extends JavaBean, V extends Comparable<V>, P extends JavaBeanComparatorBuilder<O>> {

        private final P parent;
        private final Function<O, V> getter;

        protected Query(P parent, Function<O, V> getter) {
            this.parent = parent;
            this.getter = getter;
        }

        public P parent() {
            return parent;
        }

        public V getter(O element) {
            return element == null ? null : getter.apply(element);
        }

        public P sort() {
            return addComparator(Comparator.comparing(this::getter));
        }

        public P reservedSort() {
            return addComparator(Comparator.comparing(this::getter).reversed());
        }

        public P addComparator(Comparator<O> comparator) {
            P parent = parent();
            parent.addComparator(Objects.requireNonNull(comparator));
            return parent;
        }
    }

}
