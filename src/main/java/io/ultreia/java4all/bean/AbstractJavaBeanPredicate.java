package io.ultreia.java4all.bean;

/*-
 * #%L
 * Java Beans extends by Ultreia.io
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.bean.definition.JavaBeanDefinition;
import io.ultreia.java4all.bean.definition.JavaBeanDefinitionStore;
import io.ultreia.java4all.util.Predicates;

import java.util.LinkedList;
import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;

/**
 * Created by tchemit on 11/01/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class AbstractJavaBeanPredicate<O extends JavaBean, P extends JavaBeanPredicate<O, P>> implements JavaBeanPredicate<O, P> {

    private final List<Predicate<O>> predicates;
    private final JavaBeanDefinition javaBeanDefinition;
    private Predicate<O> cachedPredicate;

    public AbstractJavaBeanPredicate(JavaBeanDefinition javaBeanDefinition) {
        this.javaBeanDefinition = javaBeanDefinition;
        this.predicates = new LinkedList<>();
    }

    protected AbstractJavaBeanPredicate(Class<? extends JavaBeanDefinition> javaBeanDefinitionType) {
        this(JavaBeanDefinitionStore.definition(javaBeanDefinitionType));
    }

    @Override
    public final boolean test(O o) {
        return predicate().test(o);
    }

    @Override
    public final void addPredicate(Predicate<O> predicate) {
        cachedPredicate = null;
        predicates.add(predicate);
    }

    @Override
    public final Predicate<O> predicate() {
        return cachedPredicate == null ? cachedPredicate = Predicates.predicate(predicates) : cachedPredicate;
    }

    protected <V> ObjectQuery<O, V, P, ?> where(String propertyName) {
        return new ObjectQuery<>(p(), this.<V>getter(propertyName));
    }

    protected <V extends Comparable<V>> SimpleComparableQuery<O, V, P, ?> whereComparable(String propertyName) {
        return new SimpleComparableQuery<>(p(), this.<V>getter(propertyName));
    }

    protected <V extends Comparable<V>> PrimitiveObjectQuery<O, V, P, ?> wherePrimitive(String propertyName) {
        return new PrimitiveObjectQuery<>(p(), this.<V>getter(propertyName));
    }

    protected ObjectBooleanQuery<O, P, ?> whereBoolean(String propertyName) {
        return new ObjectBooleanQuery<>(p(), getter(propertyName));
    }

    protected PrimitiveBooleanQuery<O, P, ?> wherePrimitiveBoolean(String propertyName) {
        return new PrimitiveBooleanQuery<>(p(), getter(propertyName));
    }

    protected StringQuery<O, P, ?> whereString(String propertyName) {
        return new StringQuery<>(p(), getter(propertyName));
    }

    protected final <V> Function<O, V> getter(String propertyName) {
        return javaBeanDefinition.<O, V>readProperty(propertyName).getter();
    }

    @SuppressWarnings("unchecked")
    protected final P p() {
        return (P) this;
    }

}

