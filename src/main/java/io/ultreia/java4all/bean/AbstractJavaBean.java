package io.ultreia.java4all.bean;

/*-
 * #%L
 * Java Beans extends by Ultreia.io
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.bean.definition.JavaBeanDefinition;
import io.ultreia.java4all.bean.definition.JavaBeanDefinitionStore;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.beans.PropertyVetoException;
import java.beans.VetoableChangeListener;
import java.beans.VetoableChangeSupport;

/**
 * Inspired from {@code swingx-common AbstractBean} but with laziness on pcs and vcs, plus implements {@link JavaBean}.
 * <p>
 * Created by tchemit on 06/01/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public abstract class AbstractJavaBean implements JavaBean {

    /**
     * Lazy helper class that manages all the property change notification machinery.
     * PropertyChangeSupport cannot be extended directly because it requires
     * a bean in the constructor, and the "this" argument is not valid until
     * after super construction. Hence, delegation instead of extension.
     *
     * @see #pcs()
     */
    private transient PropertyChangeSupport pcs;
    /**
     * Lazy helper class that manages all the veto property change notification machinery.
     *
     * @see #vcs()
     */
    private transient VetoableChangeSupport vcs;
    /**
     * Lazy helper class that manages all java bean operations.
     *
     * @see #javaBeanDefinition()
     */
    private transient JavaBeanDefinition javaBeanDefinition;

    @Override
    public JavaBeanDefinition javaBeanDefinition() {
        return javaBeanDefinition == null
                ? javaBeanDefinition = JavaBeanDefinitionStore.getDefinition(getJavaBeanType()).orElseThrow(() -> new NullPointerException("Can't find JavaBeanDefinition for " + getJavaBeanType()))
                : javaBeanDefinition;
    }

    @Override
    public final void addPropertyChangeListener(PropertyChangeListener listener) {
        pcs().addPropertyChangeListener(listener);
    }

    @Override
    public final void addPropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        pcs().addPropertyChangeListener(propertyName, listener);
    }

    @Override
    public final void removePropertyChangeListener(PropertyChangeListener listener) {
        pcs().removePropertyChangeListener(listener);
    }

    @Override
    public final void removePropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        pcs().removePropertyChangeListener(propertyName, listener);
    }

    /**
     * Returns an array of all the listeners that were added to the
     * PropertyChangeSupport object with addPropertyChangeListener().
     * <p>
     * If some listeners have been added with a named property, then
     * the returned array will be a mixture of PropertyChangeListeners
     * and <code>PropertyChangeListenerProxy</code>s. If the calling
     * method is interested in distinguishing the listeners then it must
     * test each element to see if it's a
     * <code>PropertyChangeListenerProxy</code>, perform the cast, and examine
     * the parameter.
     * <pre>
     * PropertyChangeListener[] listeners = bean.getPropertyChangeListeners();
     * for (int i = 0; i &lt; listeners.length; i++) {
     *     if (listeners[i] instanceof PropertyChangeListenerProxy) {
     *     PropertyChangeListenerProxy proxy =
     *                    (PropertyChangeListenerProxy)listeners[i];
     *     if (proxy.getPropertyName().equals("foo")) {
     *       // proxy is a PropertyChangeListener which was associated
     *       // with the property named "foo"
     *     }
     *   }
     * }
     * </pre>
     *
     * @return all of the <code>PropertyChangeListeners</code> added or an
     * empty array if no listeners have been added
     * @see java.beans.PropertyChangeListenerProxy
     */
    public final PropertyChangeListener[] getPropertyChangeListeners() {
        return pcs().getPropertyChangeListeners();
    }

    /**
     * Returns an array of all the listeners which have been associated
     * with the named property.
     *
     * @param propertyName The name of the property being listened to
     * @return all of the <code>PropertyChangeListeners</code> associated with
     * the named property.  If no such listeners have been added,
     * or if <code>propertyName</code> is null, an empty array is
     * returned.
     */
    public final PropertyChangeListener[] getPropertyChangeListeners(String propertyName) {
        return pcs().getPropertyChangeListeners(propertyName);
    }

    /**
     * Add a VetoableListener to the listener list.
     * The listener is registered for all properties.
     * The same listener object may be added more than once, and will be called
     * as many times as it is added.
     * If <code>listener</code> is null, no exception is thrown and no action
     * is taken.
     *
     * @param listener The VetoableChangeListener to be added
     */

    public final void addVetoableChangeListener(VetoableChangeListener listener) {
        vcs().addVetoableChangeListener(listener);
    }

    /**
     * Remove a VetoableChangeListener from the listener list.
     * This removes a VetoableChangeListener that was registered
     * for all properties.
     * If <code>listener</code> was added more than once to the same event
     * source, it will be notified one less time after being removed.
     * If <code>listener</code> is null, or was never added, no exception is
     * thrown and no action is taken.
     *
     * @param listener The VetoableChangeListener to be removed
     */
    public final void removeVetoableChangeListener(VetoableChangeListener listener) {
        vcs().removeVetoableChangeListener(listener);
    }

    /**
     * Returns the list of VetoableChangeListeners. If named vetoable change listeners
     * were added, then VetoableChangeListenerProxy wrappers will returned
     * <p>
     *
     * @return List of VetoableChangeListeners and VetoableChangeListenerProxys
     * if named property change listeners were added.
     */
    public final VetoableChangeListener[] getVetoableChangeListeners() {
        return vcs().getVetoableChangeListeners();
    }

    /**
     * Add a VetoableChangeListener for a specific property.  The listener
     * will be invoked only when a call on fireVetoableChange names that
     * specific property.
     * The same listener object may be added more than once.  For each
     * property,  the listener will be invoked the number of times it was added
     * for that property.
     * If <code>propertyName</code> or <code>listener</code> is null, no
     * exception is thrown and no action is taken.
     *
     * @param propertyName The name of the property to listen on.
     * @param listener     The VetoableChangeListener to be added
     */

    public final void addVetoableChangeListener(String propertyName, VetoableChangeListener listener) {
        vcs().addVetoableChangeListener(propertyName, listener);
    }

    /**
     * Remove a VetoableChangeListener for a specific property.
     * If <code>listener</code> was added more than once to the same event
     * source for the specified property, it will be notified one less time
     * after being removed.
     * If <code>propertyName</code> is null, no exception is thrown and no
     * action is taken.
     * If <code>listener</code> is null, or was never added for the specified
     * property, no exception is thrown and no action is taken.
     *
     * @param propertyName The name of the property that was listened on.
     * @param listener     The VetoableChangeListener to be removed
     */

    public final void removeVetoableChangeListener(String propertyName, VetoableChangeListener listener) {
        vcs().removeVetoableChangeListener(propertyName, listener);
    }

    /**
     * Returns an array of all the listeners which have been associated
     * with the named property.
     *
     * @param propertyName The name of the property being listened to
     * @return all the <code>VetoableChangeListeners</code> associated with
     * the named property.  If no such listeners have been added,
     * or if <code>propertyName</code> is null, an empty array is
     * returned.
     */
    public final VetoableChangeListener[] getVetoableChangeListeners(String propertyName) {
        return vcs().getVetoableChangeListeners(propertyName);
    }

    /**
     * @return the type used to acquire JavaBean definition.
     * @see JavaBeanDefinitionStore#getDefinition(Class)
     * @see #javaBeanDefinition()
     */
    protected Class getJavaBeanType() {
        return getClass();
    }

    /**
     * Report a bound property update to any registered listeners.
     * No event is fired if old and new are equal and non-null.
     * <p>
     * This is merely a convenience wrapper around the more general
     * firePropertyChange method that takes {@code
     * PropertyChangeEvent} value.
     *
     * @param propertyName The programmatic name of the property
     *                     that was changed.
     * @param oldValue     The old value of the property.
     * @param newValue     The new value of the property.
     */
    protected final void firePropertyChange(String propertyName, Object oldValue, Object newValue) {
        pcs().firePropertyChange(propertyName, oldValue, newValue);
    }

    protected final void firePropertyChange(String propertyName, Object newValue) {
        pcs().firePropertyChange(propertyName, null, newValue);
    }

    /**
     * Fire an existing PropertyChangeEvent to any registered listeners.
     * No event is fired if the given event's old and new values are
     * equal and non-null.
     *
     * @param evt The PropertyChangeEvent object.
     */
    protected final void firePropertyChange(PropertyChangeEvent evt) {
        pcs().firePropertyChange(evt);
    }

    protected final PropertyChangeSupport pcs() {
        return pcs == null ? (pcs = new PropertyChangeSupport(this)) : pcs;
    }

    protected final VetoableChangeSupport vcs() {
        return vcs == null ? (vcs = new VetoableChangeSupport(this)) : vcs;
    }

    /**
     * Report a bound indexed property update to any registered
     * listeners.
     * <p>
     * No event is fired if old and new values are equal
     * and non-null.
     * <p>
     * This is merely a convenience wrapper around the more general
     * firePropertyChange method that takes {@code PropertyChangeEvent} value.
     *
     * @param propertyName The programmatic name of the property that
     *                     was changed.
     * @param index        index of the property element that was changed.
     * @param oldValue     The old value of the property.
     * @param newValue     The new value of the property.
     */
    protected final void fireIndexedPropertyChange(String propertyName, int index, Object oldValue, Object newValue) {
        pcs().fireIndexedPropertyChange(propertyName, index, oldValue, newValue);
    }

    /**
     * Check if there are any listeners for a specific property, including
     * those registered on all properties.  If <code>propertyName</code>
     * is null, only check for listeners registered on all properties.
     *
     * @param propertyName the property name.
     * @return true if there are one or more listeners for the given property
     */
    protected final boolean hasPropertyChangeListeners(String propertyName) {
        return pcs().hasListeners(propertyName);
    }

    /**
     * Check if there are any listeners for a specific property, including
     * those registered on all properties.  If <code>propertyName</code>
     * is null, only check for listeners registered on all properties.
     *
     * @param propertyName the property name.
     * @return true if there are one or more listeners for the given property
     */
    protected final boolean hasVetoableChangeListeners(String propertyName) {
        return vcs().hasListeners(propertyName);
    }

    /**
     * Report a vetoable property update to any registered listeners.  If
     * anyone vetos the change, then fire a new event reverting everyone to
     * the old value and then rethrow the PropertyVetoException.
     * <p>
     * No event is fired if old and new are equal and non-null.
     *
     * @param propertyName The programmatic name of the property
     *                     that is about to change..
     * @param oldValue     The old value of the property.
     * @param newValue     The new value of the property.
     * @throws PropertyVetoException if the recipient wishes the property
     *                               change to be rolled back.
     */
    protected final void fireVetoableChange(String propertyName, Object oldValue, Object newValue) throws PropertyVetoException {
        vcs().fireVetoableChange(propertyName, oldValue, newValue);
    }

    /**
     * Fire a vetoable property update to any registered listeners.  If
     * anyone vetos the change, then fire a new event reverting everyone to
     * the old value and then rethrow the PropertyVetoException.
     * <p>
     * No event is fired if old and new are equal and non-null.
     *
     * @param evt The PropertyChangeEvent to be fired.
     * @throws PropertyVetoException if the recipient wishes the property
     *                               change to be rolled back.
     */
    protected final void fireVetoableChange(PropertyChangeEvent evt) throws PropertyVetoException {
        vcs().fireVetoableChange(evt);
    }
}

