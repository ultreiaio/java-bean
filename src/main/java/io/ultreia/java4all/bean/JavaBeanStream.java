package io.ultreia.java4all.bean;

/*-
 * #%L
 * Java Beans extends by Ultreia.io
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.util.Comparator;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Stream;

/**
 * Created by tchemit on 11/01/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public interface JavaBeanStream<O extends JavaBean, P extends JavaBeanStream<O, P>> extends JavaBeanPredicate<O, P> {

    void addComparator(Comparator<O> predicate);

    Optional<Comparator<O>> comparator();

    Stream<O> stream();

    default Stream<O> filter() {
        return sortedStream().filter(this);
    }

    default boolean anyMatch() {
        return stream().anyMatch(this);
    }

    default boolean allMatch() {
        return stream().allMatch(this);
    }

    default boolean noneMatch() {
        return stream().noneMatch(this);
    }

    default Stream<O> sortedStream() {
        Optional<Comparator<O>> comparator = comparator();
        return comparator.isPresent() ? stream().sorted(comparator.get()) : stream();
    }

    default Optional<O> min() {
        Optional<Comparator<O>> comparator = comparator();
        return comparator.isPresent() ? stream().min(comparator.get()) : Optional.empty();
    }

    default Optional<O> max() {
        Optional<Comparator<O>> comparator = comparator();
        return comparator.isPresent() ? stream().max(comparator.get()) : Optional.empty();
    }

    interface StreamQuery<O extends JavaBean, V, P extends JavaBeanStream<O, P>, Q extends StreamQuery<O, V, P, Q>> extends Query<O, V, P, Q> {
    }

    interface StreamComparableQuery<O extends JavaBean, V extends Comparable<V>, P extends JavaBeanStream<O, P>, Q extends StreamComparableQuery<O, V, P, Q>> extends StreamQuery<O, V, P, Q>, ComparableQuery<O, V, P, Q> {

        default P sort() {
            return addComparator(Comparator.comparing(this::getter));
        }

        default P reservedSort() {
            return addComparator(Comparator.comparing(this::getter).reversed());
        }

        default P addComparator(Comparator<O> comparator) {
            P parent = parent();
            parent.addComparator(comparator);
            return parent;
        }
    }

    class StreamObjectQuery<O extends JavaBean, V, P extends JavaBeanStream<O, P>, Q extends StreamObjectQuery<O, V, P, Q>> extends ObjectQuery<O, V, P, Q> implements StreamQuery<O, V, P, Q> {
        public StreamObjectQuery(P parent, Function<O, V> getter) {
            super(parent, getter);
        }
    }

    class StreamPrimitiveObjectQuery<O extends JavaBean, V extends Comparable<V>, P extends JavaBeanStream<O, P>, Q extends StreamPrimitiveObjectQuery<O, V, P, Q>> extends PrimitiveObjectQuery<O, V, P, Q> implements StreamComparableQuery<O, V, P, Q> {
        public StreamPrimitiveObjectQuery(P parent, Function<O, V> getter) {
            super(parent, getter);
        }
    }

    class StreamPrimitiveBooleanQuery<O extends JavaBean, P extends JavaBeanStream<O, P>, Q extends StreamPrimitiveBooleanQuery<O, P, Q>> extends PrimitiveBooleanQuery<O, P, Q> implements StreamComparableQuery<O, Boolean, P, Q> {
        public StreamPrimitiveBooleanQuery(P parent, Function<O, Boolean> getter) {
            super(parent, getter);
        }
    }

    class StreamObjectBooleanQuery<O extends JavaBean, P extends JavaBeanStream<O, P>, Q extends StreamObjectBooleanQuery<O, P, Q>> extends ObjectBooleanQuery<O, P, Q> implements StreamComparableQuery<O, Boolean, P, Q> {
        public StreamObjectBooleanQuery(P parent, Function<O, Boolean> getter) {
            super(parent, getter);
        }
    }

    class StreamSimpleComparableQuery<O extends JavaBean, V extends Comparable<V>, P extends JavaBeanStream<O, P>, Q extends StreamSimpleComparableQuery<O, V, P, Q>> extends SimpleComparableQuery<O, V, P, Q> implements StreamComparableQuery<O, V, P, Q> {
        public StreamSimpleComparableQuery(P parent, Function<O, V> getter) {
            super(parent, getter);
        }
    }

    class StreamStringQuery<O extends JavaBean, P extends JavaBeanStream<O, P>, Q extends StreamStringQuery<O, P, Q>> extends StringQuery<O, P, Q> implements StreamComparableQuery<O, String, P, Q> {
        public StreamStringQuery(P parent, Function<O, String> getter) {
            super(parent, getter);
        }
    }

}
