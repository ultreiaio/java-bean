package io.ultreia.java4all.bean.definition;

/*-
 * #%L
 * Java Beans extends by Ultreia.io
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.bean.JavaBean;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.ServiceLoader;

/**
 * Store of {@link JavaBeanDefinition}.
 * <p>
 * Use method {@link #getDefinition(Class)}, or {@link #getDefinition(JavaBean)} to get the registered java bean
 * definition (or null if none found).
 * <p>
 * Created by tchemit on 07/01/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public final class JavaBeanDefinitionStore {

    private static Map<Class<?>, JavaBeanDefinition> DEFINITIONS;

    private JavaBeanDefinitionStore() {
    }

    /**
     * @param type type of java bean
     * @return the optional java bean definition registered
     */
    public static Optional<JavaBeanDefinition> getDefinition(Class<?> type) {
        return Optional.ofNullable(definitions().get(Objects.requireNonNull(type)));
    }

    /**
     * @param javaBean java bean
     * @return the optional java bean definition registered
     */

    public static Optional<JavaBeanDefinition> getDefinition(JavaBean javaBean) {
        return Optional.ofNullable(definitions().get(Objects.requireNonNull(javaBean).getClass()));
    }

    public static Map<Class<?>, JavaBeanDefinition> definitions() {
        return DEFINITIONS == null ? DEFINITIONS = loadDefinitions() : DEFINITIONS;
    }

    public static <J extends JavaBeanDefinition> J definition(Class<J> javaBeanDefinitionType) {
        return Objects.requireNonNull(javaBeanDefinitionType).cast(definitions().values().stream().filter(d -> javaBeanDefinitionType.equals(d.getClass())).findFirst().orElseThrow());
    }

    private static synchronized Map<Class<?>, JavaBeanDefinition> loadDefinitions() {
        Map<Class<?>, JavaBeanDefinition> result = new LinkedHashMap<>();
        for (JavaBeanDefinition javaBeanDefinition : ServiceLoader.load(JavaBeanDefinition.class)) {
            for (Class<?> o : javaBeanDefinition.types()) {
                result.put(o, javaBeanDefinition);
            }
        }
        return result;
    }
}
