package io.ultreia.java4all.bean.definition;

/*-
 * #%L
 * Java Beans extends by Ultreia.io
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.bean.JavaBean;
import io.ultreia.java4all.bean.JavaBeanComparatorBuilder;
import io.ultreia.java4all.bean.JavaBeanInstanceBuilder;
import io.ultreia.java4all.bean.JavaBeanPredicate;

import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by tchemit on 07/01/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public interface JavaBeanDefinition {

    /**
     * Ge the set of accepted types using this definition.
     * This means that the method {@link JavaBeanDefinitionStore#getDefinition(Class)} will give this instance of definition
     * for any types of this set.
     *
     * @return set of accepted types using this definition
     */
    Set<Class<?>> types();

    /**
     * @return dictionary of all properties indexed by their name
     */
    Map<String, JavaBeanPropertyDefinition<?, ?>> properties();

    /**
     * @return stream of readable properties
     */
    default Stream<JavaBeanPropertyDefinition<?, ?>> readProperties() {
        return properties().values().stream().filter(JavaBeanPropertyDefinition::canRead);
    }

    /**
     * @return stream of writable properties
     */
    default Stream<JavaBeanPropertyDefinition<?, ?>> writeProperties() {
        return properties().values().stream().filter(JavaBeanPropertyDefinition::canWrite);
    }


    /**
     * @return stream of readable and writable properties
     */
    default Stream<JavaBeanPropertyDefinition<?, ?>> readAndWriteProperties() {
        return properties().values().stream().filter(JavaBeanPropertyDefinition::canReadAndWrite);
    }

    default <V> V get(String propertyName, JavaBean source) {
        JavaBeanPropertyDefinition<JavaBean, V> readDefinition = readProperty(propertyName);
        return readDefinition.get(source);
    }

    /**
     * Get a property given his name.
     *
     * @param propertyName name of property to seek
     * @param <O>          type of the java bean
     * @param <V>          type of the property
     * @return the property
     * @throws NullPointerException if no property found
     */
    @SuppressWarnings("unchecked")
    default <O extends JavaBean, V> JavaBeanPropertyDefinition<O, V> property(String propertyName) {
        return (JavaBeanPropertyDefinition<O, V>) Objects.requireNonNull(properties().get(propertyName), String.format("Can't find property %s.", propertyName));
    }

    default <V> void set(String propertyName, JavaBean target, V propertyValue) {
        JavaBeanPropertyDefinition<JavaBean, V> writeDefinition = writeProperty(propertyName);
        writeDefinition.set(target, propertyValue);
    }

    default <V> void copy(String propertyName, JavaBean source, JavaBean target) {
        boolean sameDefinition = target.javaBeanDefinition().equals(this);
        if (sameDefinition) {
            JavaBeanPropertyDefinition<JavaBean, V> readAndWriteDefinition = readAndWriteProperty(propertyName);
            readAndWriteDefinition.copy(source, target);
        } else {
            JavaBeanPropertyDefinition<JavaBean, V> readDefinition = readProperty(propertyName);
            V value = readDefinition.get(source);
            target.set(propertyName, value);
        }
    }

    @SuppressWarnings("unchecked")
    default void copy(JavaBean source, JavaBean target) {
        boolean sameDefinition = target.javaBeanDefinition().equals(this);
        if (sameDefinition) {
            readAndWriteProperties().forEach(propertyDefinition ->
                    ((JavaBeanPropertyDefinition) propertyDefinition).copy(source, target)
            );
        } else {
            Map<String, JavaBeanPropertyDefinition<?, ?>> targetProperties = target.javaBeanDefinition().properties();
            for (JavaBeanPropertyDefinition readDefinition : readProperties().collect(Collectors.toSet())) {
                JavaBeanPropertyDefinition writeDefinition = targetProperties.get(readDefinition.propertyName());
                if (writeDefinition != null && writeDefinition.canWrite()) {
                    Object value = readDefinition.get(source);
                    writeDefinition.set(target, value);
                }
            }
        }
    }

    @SuppressWarnings("unchecked")
    default void clear(JavaBean target) {
        writeProperties().forEach(propertyDefinition ->
                ((JavaBeanPropertyDefinition) propertyDefinition).clear(target)
        );
    }

    /**
     * Get a readable property given his name.
     *
     * @param propertyName name of property to seek
     * @param <O>          type of the java bean
     * @param <V>          type of the property
     * @return the property
     * @throws NullPointerException     if no property found
     * @throws IllegalArgumentException if property is not readable
     */
    default <O extends JavaBean, V> JavaBeanPropertyDefinition<O, V> readProperty(String propertyName) {
        return this.<O, V>property(propertyName).checkCanRead();
    }

    /**
     * Get a writable property given his name.
     *
     * @param propertyName name of property to seek
     * @param <O>          type of the java bean
     * @param <V>          type of the property
     * @return the property
     * @throws NullPointerException     if no property found
     * @throws IllegalArgumentException if property is not writable
     */
    default <O extends JavaBean, V> JavaBeanPropertyDefinition<O, V> writeProperty(String propertyName) {
        return this.<O, V>property(propertyName).checkCanWrite();
    }

    /**
     * Get a readable and writable property given his name.
     *
     * @param propertyName name of property to seek
     * @param <O>          type of the java bean
     * @param <V>          type of the property
     * @return the property
     * @throws NullPointerException     if no property found
     * @throws IllegalArgumentException if property is not readable, nor writable
     */
    default <O extends JavaBean, V> JavaBeanPropertyDefinition<O, V> readAndWriteProperty(String propertyName) {
        return this.<O, V>property(propertyName).checkCanRead().checkCanWrite();
    }

    default JavaBeanPredicate<?, ?> predicateBuilder() {
        throw new UnsupportedOperationException();
    }

    default JavaBeanComparatorBuilder<?> comparatorBuilder() {
        throw new UnsupportedOperationException();
    }

    default JavaBeanInstanceBuilder<?> instanceBuilder() {
        throw new UnsupportedOperationException();
    }

    <O extends JavaBean> O newInstance();

//    default <O extends JavaBean> JavaBeanStream<O> streamBuilder(Collection<O> elements) {
//        throw new UnsupportedOperationException();
//    }
}
