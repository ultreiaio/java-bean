package io.ultreia.java4all.bean.definition;

/*-
 * #%L
 * Java Beans extends by Ultreia.io
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.bean.JavaBean;

import java.util.Objects;
import java.util.function.BiConsumer;
import java.util.function.Function;

/**
 * Created by tchemit on 13/01/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public final class JavaBeanPropertyDefinition<O extends JavaBean, V> {

    private final String propertyName;
    private final Class<V> type;
    private final Function<O, V> getter;
    private final BiConsumer<O, V> setter;
    private final V defaultValue;

    JavaBeanPropertyDefinition(String propertyName, Class<V> type, Function<O, V> getter, BiConsumer<O, V> setter) {
        this.propertyName = Objects.requireNonNull(propertyName);
        this.type = Objects.requireNonNull(type);
        this.getter = getter;
        this.setter = setter;
        this.defaultValue = type.isPrimitive() ? defaultValue(type) : null;
    }

    public JavaBeanPropertyDefinition<O, V> checkCanRead() {
        if (!canRead()) {
            throw new IllegalArgumentException("Property " + propertyName + " is not readable.");
        }
        return this;
    }

    public JavaBeanPropertyDefinition<O, V> checkCanWrite() {
        if (!canWrite()) {
            throw new IllegalArgumentException("Property " + propertyName + " is not writable.");
        }
        return this;
    }

    public String propertyName() {
        return propertyName;
    }

    public Class<?> type() {
        return type;
    }

    public Function<O, V> getter() {
        return getter;
    }

    public BiConsumer<O, V> setter() {
        return setter;
    }

    public V defaultValue() {
        return defaultValue;
    }

    public boolean canRead() {
        return getter != null;
    }

    public boolean canWrite() {
        return setter != null;
    }

    public boolean canReadAndWrite() {
        return canRead() && canWrite();
    }

    public V get(O javaBean) {
        return getter.apply(javaBean);
    }

    public void set(O javaBean, V value) {
        setter.accept(javaBean, value);
    }

    public void copy(O source, O target) {
        V value = get(source);
        set(target, value);
    }

    public void clear(O javaBean) {
        set(javaBean, defaultValue);
    }

    private static final Byte BYTE_DEFAULT = '\0';
    private static final Character CHAR_DEFAULT = '0';
    private static final Integer INT_DEFAULT = 0;
    private static final Double DOUBLE_DEFAULT = 0d;
    private static final Float FLOAT_DEFAULT = 0f;
    private static final Long LONG_DEFAULT = 0l;
    private static final Short SHORT_DEFAULT = 0;

    @SuppressWarnings("unchecked")
    public static <T> T defaultValue(Class<T> type) {
        Objects.requireNonNull(type);
        if (type.isPrimitive()) {
            if (type == boolean.class) {
                return (T) Boolean.FALSE;
            } else if (type == char.class) {
                return (T) CHAR_DEFAULT;
            } else if (type == byte.class) {
                return (T) BYTE_DEFAULT;
            } else if (type == short.class) {
                return (T) SHORT_DEFAULT;
            } else if (type == int.class) {
                return (T) INT_DEFAULT;
            } else if (type == long.class) {
                return (T) LONG_DEFAULT;
            } else if (type == float.class) {
                return (T) FLOAT_DEFAULT;
            } else if (type == double.class) {
                return (T) DOUBLE_DEFAULT;
            }
        }
        return null;
    }
}
