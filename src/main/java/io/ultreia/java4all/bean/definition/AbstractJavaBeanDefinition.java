package io.ultreia.java4all.bean.definition;

/*-
 * #%L
 * Java Beans extends by Ultreia.io
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.bean.JavaBean;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public abstract class AbstractJavaBeanDefinition implements JavaBeanDefinition {

    private Set<Class<?>> acceptedTypes;
    private Map<String, JavaBeanPropertyDefinition<?, ?>> properties;
    private Class<?> mainType;

    @Override
    public final Set<Class<?>> types() {
        return acceptedTypes == null ? acceptedTypes = Objects.requireNonNull(loadAcceptedTypes()) : acceptedTypes;
    }

    protected abstract Set<Class<?>> loadAcceptedTypes();

    protected abstract Map<String, Class<?>> loadTypes();

    protected abstract Map<String, Function<?, ?>> loadGetters();

    protected abstract Map<String, BiConsumer<?, ?>> loadSetters();

    @Override
    public final Map<String, JavaBeanPropertyDefinition<?, ?>> properties() {
        if (properties == null) {
            Map<String, Function<?, ?>> getters = Objects.requireNonNull(loadGetters());
            Map<String, BiConsumer<?, ?>> setters = Objects.requireNonNull(loadSetters());
            Map<String, Class<?>> types = Objects.requireNonNull(loadTypes());
            List<String> allPropertyNames = Stream.concat(getters.keySet().stream(), setters.keySet().stream()).distinct().sorted(String::compareTo).collect(Collectors.toList());
            Map<String, JavaBeanPropertyDefinition<?, ?>> propertiesBuilder = new LinkedHashMap<>();
            for (String propertyName : allPropertyNames) {
                Class<?> type = types.get(propertyName);
                propertiesBuilder.put(propertyName, newProperty(propertyName, type, getters, setters));
            }
            this.properties = Collections.unmodifiableMap(propertiesBuilder);
        }
        return properties;
    }

    @SuppressWarnings("unchecked")
    @Override
    public final <O extends JavaBean> O newInstance() {
        try {
            return (O) mainType().getDeclaredConstructor().newInstance();
        } catch (InstantiationException | IllegalAccessException | NoSuchMethodException |
                 InvocationTargetException e) {
            throw new IllegalStateException("Can't instantiate with: " + mainType, e.getCause());
        }
    }

    private <O extends JavaBean, V> JavaBeanPropertyDefinition<O, V> newProperty(String propertyName, Class<V> type, Map<String, Function<?, ?>> getters, Map<String, BiConsumer<?, ?>> setters) {
        @SuppressWarnings("unchecked") Function<O, V> getter = (Function<O, V>) getters.get(propertyName);
        @SuppressWarnings("unchecked") BiConsumer<O, V> setter = (BiConsumer<O, V>) setters.get(propertyName);
        return new JavaBeanPropertyDefinition<>(propertyName, type, getter, setter);

    }

    private Class<?> mainType() {
        if (mainType == null) {
            for (Class<?> acceptedType : acceptedTypes) {
                if (acceptedType.isInterface() || Modifier.isAbstract(acceptedType.getModifiers())) {
                    continue;
                }
                mainType = acceptedType;
            }
            Objects.requireNonNull(mainType, "Can't find a type to instantiate in: " + acceptedTypes);
        }
        return mainType;
    }
}
