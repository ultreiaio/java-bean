package io.ultreia.java4all.bean;

/*-
 * #%L
 * Java Beans extends by Ultreia.io
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.bean.definition.JavaBeanDefinition;
import io.ultreia.java4all.bean.definition.JavaBeanDefinitionStore;
import io.ultreia.java4all.util.Comparators;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

/**
 * Created by tchemit on 11/01/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class AbstractJavaBeanComparatorBuilder<O extends JavaBean, P extends JavaBeanComparatorBuilder<O>> implements JavaBeanComparatorBuilder<O> {

    private final List<Comparator<O>> comparators;
    private final JavaBeanDefinition javaBeanDefinition;

    public AbstractJavaBeanComparatorBuilder(JavaBeanDefinition javaBeanDefinition) {
        this.javaBeanDefinition = javaBeanDefinition;
        this.comparators = new LinkedList<>();
    }

    protected AbstractJavaBeanComparatorBuilder(Class<? extends JavaBeanDefinition> javaBeanDefinitionType) {
        this(JavaBeanDefinitionStore.definition(javaBeanDefinitionType));
    }

    @Override
    public void addComparator(Comparator<O> predicate) {
        comparators.add(predicate);
    }

    @Override
    public Optional<Comparator<O>> build() {
        return Comparators.comparator(comparators);
    }

    protected <V extends Comparable<V>> Query<O, V, P> on(String propertyName) {
        return new Query<>(p(), this.<V>getter(propertyName));
    }

    protected <V> Function<O, V> getter(String propertyName) {
        return javaBeanDefinition.<O, V>readProperty(propertyName).getter();
    }

    @SuppressWarnings("unchecked")
    protected P p() {
        return (P) this;
    }

}

