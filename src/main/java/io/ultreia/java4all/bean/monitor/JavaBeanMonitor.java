package io.ultreia.java4all.bean.monitor;

/*-
 * #%L
 * Java Beans extends by Ultreia.io
 * %%
 * Copyright (C) 2018 - 2023 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.bean.JavaBean;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;

/**
 * To monitor a java bean.
 * <p>
 * You indicates which properties to monitor (via the constructor).
 * <p>
 * Then attach a bean to monitor via the method {@link #setBean(JavaBean)}.
 * <p>
 * The method {@link #clearModified()} reset the states about modified properties.
 * <p>
 * The method {@link #wasModified()} tells if there was any modification on
 * monitored properties for the attached bean since the last call to
 * {@link #clearModified()} (or {@link #setBean(JavaBean)}.
 * <p>
 * the method {@link #getModifiedProperties()} gives you the names of monitored
 * properties for the attached bean since the last call to
 * {@link #clearModified()} (or {@link #setBean(JavaBean)}.
 * <p>
 * the method {@link #getModification(String)} returns the optional (immutable) property modification.
 * <p>
 * Created on 21/02/2023.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 0.2.2
 */
public class JavaBeanMonitor {
    private final PropertyChangeListener listener = this::propertyChange;
    /**
     * Names of properties to watch on attached bean.
     */
    protected final List<String> propertyNames;
    /**
     * Names of monitored and modified properties for the attached bean.
     */
    protected final Map<String, JavaBeanPropertyModification> modificationsByPropertyName;
    /**
     * The bean to monitor.
     */
    protected JavaBean bean;

    /**
     * Constructor of monitor with property names to monitor.
     *
     * @param propertyNames the names of properties to monitor
     */
    public JavaBeanMonitor(String... propertyNames) {
        this.propertyNames = List.of(propertyNames);
        this.modificationsByPropertyName = new LinkedHashMap<>();
    }

    /**
     * Obtains the monitored {@link #bean}.
     *
     * @return the attached bean, or {@code null} if none attached.
     */
    public JavaBean getBean() {
        return bean;
    }

    /**
     * Sets the {@code bean} to monitor.
     * <p>
     * As a side effect, it will attach the {@code propertyChangeListener} to new bean
     * (if not null) and remove it from the previous bean attached (if not null).
     * <p>
     * As a second side effect, it will always clean the modified states,
     * using the method {@link #clearModified()}.
     *
     * @param bean the new bean to monitor
     */
    public void setBean(JavaBean bean) {
        JavaBean oldBean = getBean();
        this.bean = bean;

        // while removing a bean must clean modified states
        clearModified();

        if (oldBean != null) {
            // remove listener from old bean
            oldBean.removePropertyChangeListener(listener);
        }
        if (bean != null) {
            // add listener to new bean
            bean.addPropertyChangeListener(listener);
        }
    }

    /**
     * Tells if any of the monitored properties were modified on the attached
     * bean since last call to {@link #clearModified()} or
     * {@link #setBean(JavaBean)}.
     *
     * @return {@code true} if there were a modified monitored property on
     * the attached bean, {@code false} otherwise.
     */
    public boolean wasModified() {
        return !modificationsByPropertyName.isEmpty();
    }

    /**
     * Obtains the names of monitored properties which has been touched for the
     * attached bean since last call to {@link #clearModified()} or
     * {@link #setBean(JavaBean)}.
     *
     * @return the set of property names modified on the bean used by monitor.
     */
    public Set<String> getModifiedProperties() {
        Set<String> propertyNames = modificationsByPropertyName.keySet();
        return Set.copyOf(propertyNames);
    }

    /**
     * Obtains the property modification if exists as an immutable java bean property
     * modification to avoid any side effects.
     *
     * @param propertyName name of property to seek
     * @return an optional of the property modification if found, otherwise an empty optional.
     */
    public Optional<JavaBeanPropertyModification> getModification(String propertyName) {
        return Optional.ofNullable(modificationsByPropertyName.get(Objects.requireNonNull(propertyName))).map(JavaBeanPropertyModification::toImmutable);
    }

    /**
     * To clear the modified states.
     */
    public void clearModified() {
        modificationsByPropertyName.clear();
    }

    public <R extends JavaBeanModifications> Optional<R> toModifications(Function<Set<JavaBeanPropertyModification>, R> newInstance) {
        return wasModified() ? Optional.of(newInstance.apply(Set.copyOf(modificationsByPropertyName.values()))) : Optional.empty();
    }

    /**
     * To apply on the given {@code bean}, all modifications registered in the monitor, with optional clear of
     * the monitor.
     *
     * @param bean  bean on which apply modifications
     * @param clear should we clear the monitor after the operation?
     */
    public void apply(JavaBean bean, boolean clear) {
        List.copyOf(modificationsByPropertyName.values()).forEach(propertyDiff -> propertyDiff.applyNewValue(bean));
        if (clear) {
            clearModified();
        }
    }

    /**
     * To reset on the given {@code bean}, all modifications registered in the monitor, with optional clear of
     * the monitor.
     *
     * @param bean  bean on which reset modifications
     * @param clear should we clear the monitor after the operation?
     */
    public void reset(JavaBean bean, boolean clear) {
        List.copyOf(modificationsByPropertyName.values()).forEach(propertyDiff -> propertyDiff.applyOldValue(bean));
        if (clear) {
            clearModified();
        }
    }

    private void propertyChange(PropertyChangeEvent evt) {
        String propertyName = evt.getPropertyName();
        if (!propertyNames.contains(propertyName)) {
            // the property is not monitored, nothing to do
            return;
        }

        // the property is monitored

        Object newValue = evt.getNewValue();
        Object oldValue = evt.getOldValue();

        if (Objects.equals(oldValue, newValue)) {
            // in fact property has no real change, nothing to do
            return;
        }
        if (modificationsByPropertyName.containsKey(propertyName)) {

            // property was already modified
            JavaBeanPropertyModification propertyDiff = modificationsByPropertyName.get(propertyName);

            // check if value did not come back to original

            if (Objects.equals(propertyDiff.getOldValue(), newValue)) {

                // coming back to original value
                // the property is no more modified
                modificationsByPropertyName.remove(propertyName);
                return;
            }

            // change the target value
            propertyDiff.setNewValue(newValue);
            return;
        }

        // property was not modified, add it to modified properties
        modificationsByPropertyName.put(propertyName, new JavaBeanPropertyModification(propertyName, oldValue, newValue));
    }
}

