package io.ultreia.java4all.bean.monitor;

/*-
 * #%L
 * Java Beans extends by Ultreia.io
 * %%
 * Copyright (C) 2018 - 2023 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.bean.JavaBean;

import java.util.Objects;

/**
 * To describe a java bean property modification.
 * <p>
 * Created on 21/02/2023.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 0.2.2
 */
public class JavaBeanPropertyModification {

    private final String propertyName;
    private Object oldValue;
    private Object newValue;

    public JavaBeanPropertyModification(String propertyName, Object oldValue, Object newValue) {
        this.propertyName = Objects.requireNonNull(propertyName);
        this.oldValue = oldValue;
        this.newValue = newValue;
    }

    public String getPropertyName() {
        return propertyName;
    }

    public Object getOldValue() {
        return oldValue;
    }

    public void setOldValue(Object oldValue) {
        this.oldValue = oldValue;
    }

    public Object getNewValue() {
        return newValue;
    }

    public void setNewValue(Object newValue) {
        this.newValue = newValue;
    }

    public void applyOldValue(JavaBean bean) {
        bean.set(getPropertyName(), getOldValue());
    }

    public void applyNewValue(JavaBean bean) {
        bean.set(getPropertyName(), getNewValue());
    }

    public JavaBeanPropertyModification toImmutable() {
        return new JavaBeanPropertyModification(propertyName, oldValue, newValue) {
            @Override
            public void setOldValue(Object oldValue) {
                throw new IllegalStateException("You can not call this method on a immutable version.");
            }

            @Override
            public void setNewValue(Object newValue) {
                throw new IllegalStateException("You can not call this method on a immutable version.");
            }

            @Override
            public JavaBeanPropertyModification toImmutable() {
                return this;
            }
        };
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof JavaBeanPropertyModification)) {
            return false;
        }
        JavaBeanPropertyModification that = (JavaBeanPropertyModification) o;
        return propertyName.equals(that.propertyName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(propertyName);
    }
}
