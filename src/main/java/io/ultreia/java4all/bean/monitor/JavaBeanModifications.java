package io.ultreia.java4all.bean.monitor;

/*-
 * #%L
 * Java Beans extends by Ultreia.io
 * %%
 * Copyright (C) 2018 - 2023 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.bean.JavaBean;

import java.util.Objects;
import java.util.Set;

/**
 * To keep some modifications on a JavaBean.
 * <p>
 * Created on 21/02/2023.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 0.2.2
 */
public class JavaBeanModifications {

    private final Set<JavaBeanPropertyModification> modifications;

    public JavaBeanModifications(Set<JavaBeanPropertyModification> modifications) {
        this.modifications = Objects.requireNonNull(modifications);
    }

    public Set<JavaBeanPropertyModification> getModifications() {
        return Set.copyOf(modifications);
    }

    /**
     * To apply on the given {@code bean}, all modifications registered in the result.
     *
     * @param bean bean on which apply modifications
     */
    public void flushToBean(JavaBean bean) {
        modifications.forEach(propertyDiff -> propertyDiff.applyNewValue(bean));
    }

    /**
     * To reset on the given {@code bean}, all modifications registered in the result.
     *
     * @param bean bean on which reset modifications
     */
    public void reset(JavaBean bean) {
        modifications.forEach(propertyDiff -> propertyDiff.applyOldValue(bean));
    }
}
