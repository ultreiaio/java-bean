package io.ultreia.java4all.bean;

/*-
 * #%L
 * Java Beans extends by Ultreia.io
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.bean.definition.JavaBeanDefinition;
import io.ultreia.java4all.bean.definition.JavaBeanDefinitionStore;
import io.ultreia.java4all.util.Comparators;

import java.util.Collection;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

/**
 * Created by tchemit on 11/01/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class AbstractJavaBeanStream<O extends JavaBean, P extends JavaBeanStream<O, P>> extends AbstractJavaBeanPredicate<O, P> implements JavaBeanStream<O, P> {

    private final List<Comparator<O>> comparators;
    private final Collection<O> elements;

    public AbstractJavaBeanStream(JavaBeanDefinition javaBeanDefinition, Collection<O> elements) {
        super(javaBeanDefinition);
        this.elements = elements;
        this.comparators = new LinkedList<>();
    }

    protected AbstractJavaBeanStream(Class<? extends JavaBeanDefinition> javaBeanDefinitionType, Collection<O> elements) {
        this(JavaBeanDefinitionStore.definition(javaBeanDefinitionType), elements);
    }

    @Override
    public void addComparator(Comparator<O> predicate) {
        comparators.add(predicate);
    }

    @Override
    public Optional<Comparator<O>> comparator() {
        return Comparators.comparator(comparators);
    }

    @Override
    public Stream<O> stream() {
        return elements.stream();
    }

    protected <V> StreamObjectQuery<O, V, P, ?> where(String propertyName) {
        return new StreamObjectQuery<>(p(), this.<V>getter(propertyName));
    }

    protected <V extends Comparable<V>> StreamSimpleComparableQuery<O, V, P, ?> whereComparable(String propertyName) {
        return new StreamSimpleComparableQuery<>(p(), this.<V>getter(propertyName));
    }

    protected <V extends Comparable<V>> StreamPrimitiveObjectQuery<O, V, P, ?> wherePrimitive(String propertyName) {
        return new StreamPrimitiveObjectQuery<>(p(), this.<V>getter(propertyName));
    }

    protected StreamObjectBooleanQuery<O, P, ?> whereBoolean(String propertyName) {
        return new StreamObjectBooleanQuery<>(p(), getter(propertyName));
    }

    protected StreamPrimitiveBooleanQuery<O, P, ?> wherePrimitiveBoolean(String propertyName) {
        return new StreamPrimitiveBooleanQuery<>(p(), getter(propertyName));
    }

    protected StreamStringQuery<O, P, ?> whereString(String propertyName) {
        return new StreamStringQuery<>(p(), getter(propertyName));
    }

}

