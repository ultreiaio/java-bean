package io.ultreia.java4all.bean;

/*-
 * #%L
 * Java Beans extends by Ultreia.io
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.bean.definition.JavaBeanDefinition;

import java.beans.PropertyChangeListener;
import java.util.Objects;

/**
 * Created by tchemit on 11/11/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public interface JavaBean extends GetterProducer, SetterProducer {

    /**
     * @return definition of this object
     */
    JavaBeanDefinition javaBeanDefinition();

    @Override
    default <O> O get(String propertyName) {
        return javaBeanDefinition().get(Objects.requireNonNull(propertyName), this);
    }

    @Override
    default <O> void set(String propertyName, O propertyValue) {
        javaBeanDefinition().set(Objects.requireNonNull(propertyName), this, propertyValue);
    }

    /**
     * Copy given property to target, this a shortcut to {@code target.set(propertyName, this.get(propertyName))}.
     *
     * @param propertyName name of property to cpoy
     * @param target       where to copy
     * @throws NullPointerException if arguments are null
     */
    default void copy(String propertyName, JavaBean target) {
        javaBeanDefinition().copy(Objects.requireNonNull(propertyName), this, Objects.requireNonNull(target));
    }

    /**
     * Copy all compatible properties from this object to given {@code target}.
     *
     * @param target where to copy
     */
    default void copy(JavaBean target) {
        javaBeanDefinition().copy(this, Objects.requireNonNull(target));
    }

    /**
     * Clear all writable properties of this object.
     */
    default void clear() {
        javaBeanDefinition().clear(this);
    }

    /**
     * @return predicate builder
     */
    default JavaBeanPredicate<?, ?> predicateBuilder() {
        return javaBeanDefinition().predicateBuilder();
    }

    /**
     * @return comparator builder
     */
    default JavaBeanComparatorBuilder<?> comparatorBuilder() {
        return javaBeanDefinition().comparatorBuilder();
    }

    /**
     * @return instance builder
     */
    default JavaBeanInstanceBuilder<?> instanceBuilder() {
        return javaBeanDefinition().instanceBuilder();
    }

    /**
     * Add a PropertyChangeListener for a specific property.  The listener
     * will be invoked only when a call on firePropertyChange names that
     * specific property.
     * The same listener object may be added more than once.  For each
     * property,  the listener will be invoked the number of times it was added
     * for that property.
     * If <code>propertyName</code> or <code>listener</code> is null, no
     * exception is thrown and no action is taken.
     *
     * @param propertyName The name of the property to listen on.
     * @param listener     The PropertyChangeListener to be added
     */
    void addPropertyChangeListener(String propertyName, PropertyChangeListener listener);

    /**
     * Add a PropertyChangeListener to the listener list.
     * The listener is registered for all properties.
     * The same listener object may be added more than once, and will be called
     * as many times as it is added.
     * If <code>listener</code> is null, no exception is thrown and no action
     * is taken.
     *
     * @param listener The PropertyChangeListener to be added
     */
    void addPropertyChangeListener(PropertyChangeListener listener);

    /**
     * Remove a PropertyChangeListener for a specific property.
     * If <code>listener</code> was added more than once to the same event
     * source for the specified property, it will be notified one less time
     * after being removed.
     * If <code>propertyName</code> is null,  no exception is thrown and no
     * action is taken.
     * If <code>listener</code> is null, or was never added for the specified
     * property, no exception is thrown and no action is taken.
     *
     * @param propertyName The name of the property that was listened on.
     * @param listener     The PropertyChangeListener to be removed
     */
    void removePropertyChangeListener(String propertyName, PropertyChangeListener listener);


    /**
     * Remove a PropertyChangeListener from the listener list.
     * This removes a PropertyChangeListener that was registered
     * for all properties.
     * If <code>listener</code> was added more than once to the same event
     * source, it will be notified one less time after being removed.
     * If <code>listener</code> is null, or was never added, no exception is
     * thrown and no action is taken.
     *
     * @param listener The PropertyChangeListener to be removed
     */
    void removePropertyChangeListener(PropertyChangeListener listener);

}
