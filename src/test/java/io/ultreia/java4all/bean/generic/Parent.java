package io.ultreia.java4all.bean.generic;

/*-
 * #%L
 * Java Beans extends by Ultreia.io
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.bean.AbstractJavaBean;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by tchemit on 01/10/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class Parent<O extends Serializable> extends AbstractJavaBean {

    private List<O> value;
    private Map<Date, O> date;

    public List<O> getValue() {
        return value;
    }

    public void setValue(List<O> value) {
        this.value = value;
    }

    public Map<Date, O> getDate() {
        return date;
    }

    public void setDate(Map<Date, O> date) {
        Map<Date, O> oldValue = getDate();
        this.date = date;
        firePropertyChange("date", oldValue, date);
    }
}
