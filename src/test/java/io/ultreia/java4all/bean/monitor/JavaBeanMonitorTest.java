package io.ultreia.java4all.bean.monitor;

/*-
 * #%L
 * Java Beans extends by Ultreia.io
 * %%
 * Copyright (C) 2018 - 2023 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.bean.MyJavaBean1;
import org.junit.Assert;
import org.junit.Test;

import java.util.Optional;

/**
 * Created on 21/02/2023.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 0.2.2
 */
public class JavaBeanMonitorTest {

    @Test
    public void wasModified() {
        MyJavaBean1 bean = new MyJavaBean1();
        JavaBeanMonitor monitor = new JavaBeanMonitor("one", "two", "primitiveBoolean");

        Assert.assertFalse(monitor.wasModified());
        monitor.setBean(bean);
        Assert.assertFalse(monitor.wasModified());

        bean.setOne("One");
        Assert.assertTrue(monitor.wasModified());

        bean.setOne(null);
        Assert.assertFalse(monitor.wasModified());

        bean.setPrimitiveBoolean(true);
        Assert.assertTrue(monitor.wasModified());
        bean.setPrimitiveBoolean(false);
        Assert.assertFalse(monitor.wasModified());
    }

    @Test
    public void clearModified() {
        MyJavaBean1 bean = new MyJavaBean1();
        JavaBeanMonitor monitor = new JavaBeanMonitor("one", "two", "primitiveBoolean");
        monitor.setBean(bean);
        bean.setOne("One");
        bean.setPrimitiveBoolean(true);
        Assert.assertTrue(monitor.wasModified());
        Assert.assertEquals(2, monitor.getModifiedProperties().size());

        monitor.clearModified();
        Assert.assertFalse(monitor.wasModified());
        Assert.assertEquals(0, monitor.getModifiedProperties().size());
    }

    @Test
    public void toModifications() {
        MyJavaBean1 bean = new MyJavaBean1();
        JavaBeanMonitor monitor = new JavaBeanMonitor("one", "two", "primitiveBoolean");
        monitor.setBean(bean);
        bean.setOne("One");
        bean.setPrimitiveBoolean(true);

        JavaBeanModifications modifications = monitor.toModifications(JavaBeanModifications::new).orElse(null);
        Assert.assertNotNull(modifications);
        Assert.assertEquals(2, modifications.getModifications().size());

        monitor.clearModified();
        Assert.assertNull(monitor.toModifications(JavaBeanModifications::new).orElse(null));
    }


    @Test
    public void getModification() {
        MyJavaBean1 bean = new MyJavaBean1();
        JavaBeanMonitor monitor = new JavaBeanMonitor("one", "two", "primitiveBoolean");
        monitor.setBean(bean);
        bean.setOne("One");
        bean.setPrimitiveBoolean(true);

        Optional<JavaBeanPropertyModification> optionalModification;

        optionalModification = monitor.getModification("one");

        Assert.assertNotNull(optionalModification);
        Assert.assertTrue(optionalModification.isPresent());
        JavaBeanPropertyModification modification = optionalModification.get();

        Assert.assertNull(modification.getOldValue());
        Assert.assertEquals("one", modification.getPropertyName());
        Assert.assertEquals("One", modification.getNewValue());

        optionalModification = monitor.getModification("two");
        Assert.assertNotNull(optionalModification);
        Assert.assertTrue(optionalModification.isEmpty());

    }
}
