package io.ultreia.java4all.bean;

/*-
 * #%L
 * Java Beans extends by Ultreia.io
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.bean.spi.GenerateJavaBeanDefinition;

/**
 * Created by tchemit on 07/01/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
@GenerateJavaBeanDefinition(comparator = true, instance = true, predicate = true, stream = true)
public class MyJavaBean1 extends MyJavaBean {

    private String one;
    private String two;
    private String three;
    private boolean primitiveBoolean;
    private Boolean bigBoolean;
    private int primitiveInt;
    private Integer bigInteger;

    public String getOne() {
        return one;
    }

    public String getTwo() {
        return two;
    }

    public boolean isPrimitiveBoolean() {
        return primitiveBoolean;
    }

    public Boolean getBigBoolean() {
        return bigBoolean;
    }

    public int getPrimitiveInt() {
        return primitiveInt;
    }

    public Integer getBigInteger() {
        return bigInteger;
    }

    protected String getThree() {
        return three;
    }

    public void setOne(String one) {
        String oldValue = this.one;
        this.one = one;
        firePropertyChange("one", oldValue, one);
    }

    public void setTwo(String two) {
        String oldValue = this.two;
        this.two = two;
        firePropertyChange("two", oldValue, two);
    }

    protected void setThree(String three) {
        String oldValue = this.three;
        this.three = three;
        firePropertyChange("three", oldValue, three);
    }

    public void setPrimitiveBoolean(boolean primitiveBoolean) {
        boolean oldValue = this.primitiveBoolean;
        this.primitiveBoolean = primitiveBoolean;
        firePropertyChange("primitiveBoolean", oldValue, primitiveBoolean);
    }

    public void setBigBoolean(Boolean bigBoolean) {
        Boolean oldValue = this.bigBoolean;
        this.bigBoolean = bigBoolean;
        firePropertyChange("bigBoolean", oldValue, bigBoolean);
    }

    public void setPrimitiveInt(int primitiveInt) {
        int oldValue = this.primitiveInt;
        this.primitiveInt = primitiveInt;
        firePropertyChange("primitiveInt", oldValue, primitiveInt);
    }

    public void setBigInteger(Integer bigInteger) {
        Integer oldValue = this.bigInteger;
        this.bigInteger = bigInteger;
        firePropertyChange("bigInteger", oldValue, bigInteger);
    }
}
