package io.ultreia.java4all.bean;

/*-
 * #%L
 * Java Beans extends by Ultreia.io
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.bean.spi.GenerateJavaBeanDefinition;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by tchemit on 13/01/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class JavaBeanTest {

    @GenerateJavaBeanDefinition(comparator = true,instance = true,predicate = true,stream = true)
    public static class Bean1 extends AbstractJavaBean {
        private String stringProperty;
        private Boolean booleanProperty;

        public String getStringProperty() {
            return stringProperty;
        }

        public void setStringProperty(String stringProperty) {
            this.stringProperty = stringProperty;
        }

        public Boolean getBooleanProperty() {
            return booleanProperty;
        }

        public void setBooleanProperty(Boolean booleanProperty) {
            this.booleanProperty = booleanProperty;
        }
    }

    @GenerateJavaBeanDefinition(comparator = true,instance = true,predicate = true,stream = true)
    public static class Bean2 extends AbstractJavaBean {
        private String stringProperty1;
        private Boolean booleanProperty;
        private Class<?> classProperty;

        public String getStringProperty1() {
            return stringProperty1;
        }

        public void setStringProperty1(String stringProperty1) {
            this.stringProperty1 = stringProperty1;
        }

        public Boolean getBooleanProperty() {
            return booleanProperty;
        }

        public void setBooleanProperty(Boolean booleanProperty) {
            this.booleanProperty = booleanProperty;
        }

        public Class<?> getClassProperty() {
            return classProperty;
        }

        public void setClassProperty(Class<?> classProperty) {
            this.classProperty = classProperty;
        }
    }

    @Test
    public void copy() {
        Bean1 bean1 = new Bean1();
        bean1.setStringProperty("One");
        bean1.setBooleanProperty(true);
        Bean2 bean2 = new Bean2();

        Assert.assertNull(bean2.getStringProperty1());
        Assert.assertNull(bean2.getBooleanProperty());
        bean1.copy(bean2);
        Assert.assertNull(bean2.getStringProperty1());
        Assert.assertEquals(true, bean2.getBooleanProperty());
    }

    @Test
    public void instance() {
        Bean1 bean1 = Bean1JavaBeanDefinition.instance().stringProperty("One").booleanProperty(true).build();
        Assert.assertNotNull(bean1.getStringProperty());
        Assert.assertNotNull(bean1.getBooleanProperty());

        Bean2 bean2 = Bean2JavaBeanDefinition.instance().build();

        Assert.assertNull(bean2.getStringProperty1());
        Assert.assertNull(bean2.getBooleanProperty());
    }
}
