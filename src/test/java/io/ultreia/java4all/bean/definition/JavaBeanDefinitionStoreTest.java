package io.ultreia.java4all.bean.definition;

/*-
 * #%L
 * Java Beans extends by Ultreia.io
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.bean.BeanWithGenerics;
import io.ultreia.java4all.bean.JavaBeanTest;
import io.ultreia.java4all.bean.MyJavaBean1;
import io.ultreia.java4all.bean.MyJavaBean2;
import io.ultreia.java4all.bean.MyJavaBean3;
import org.junit.Assert;
import org.junit.Test;

import java.util.Map;

/**
 * Created by tchemit on 07/01/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class JavaBeanDefinitionStoreTest {

    @Test
    public void getDefinitions() {
        Map<Class<?>, JavaBeanDefinition> definitions = JavaBeanDefinitionStore.definitions();
        Assert.assertNotNull(definitions);
        Assert.assertEquals(8, definitions.size());
        Assert.assertNotNull(definitions.get(MyJavaBean1.class));
        Assert.assertNotNull(definitions.get(MyJavaBean2.class));
        Assert.assertNotNull(definitions.get(MyJavaBean3.class));
        Assert.assertNotNull(definitions.get(JavaBeanTest.Bean1.class));
        Assert.assertNotNull(definitions.get(JavaBeanTest.Bean2.class));
        Assert.assertNotNull(definitions.get(BeanWithGenerics.class));
    }

    @Test
    public void getDefinitionByType() {
        Assert.assertNotNull(JavaBeanDefinitionStore.getDefinition(MyJavaBean1.class));
        Assert.assertNotNull(JavaBeanDefinitionStore.getDefinition(MyJavaBean2.class));
        Assert.assertNotNull(JavaBeanDefinitionStore.getDefinition(MyJavaBean3.class));
        Assert.assertNotNull(JavaBeanDefinitionStore.getDefinition(JavaBeanTest.Bean1.class));
        Assert.assertNotNull(JavaBeanDefinitionStore.getDefinition(JavaBeanTest.Bean2.class));
        Assert.assertNotNull(JavaBeanDefinitionStore.getDefinition(BeanWithGenerics.class));
    }

    @Test
    public void getDefinitionByJavaBean() {
        Assert.assertNotNull(JavaBeanDefinitionStore.getDefinition(new MyJavaBean1()));
        Assert.assertNotNull(JavaBeanDefinitionStore.getDefinition(new MyJavaBean2()));
        Assert.assertNotNull(JavaBeanDefinitionStore.getDefinition(new MyJavaBean3()));
        Assert.assertNotNull(JavaBeanDefinitionStore.getDefinition(new JavaBeanTest.Bean1()));
        Assert.assertNotNull(JavaBeanDefinitionStore.getDefinition(new JavaBeanTest.Bean2()));
        Assert.assertNotNull(JavaBeanDefinitionStore.getDefinition(new BeanWithGenerics<>()));

        Assert.assertNotNull(new MyJavaBean1().javaBeanDefinition());
        Assert.assertNotNull(new MyJavaBean2().javaBeanDefinition());
        Assert.assertNotNull(new MyJavaBean3().javaBeanDefinition());
        Assert.assertNotNull(new JavaBeanTest.Bean1().javaBeanDefinition());
        Assert.assertNotNull(new JavaBeanTest.Bean2().javaBeanDefinition());
        Assert.assertNotNull(new BeanWithGenerics<>().javaBeanDefinition());
    }
}
