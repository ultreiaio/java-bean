package io.ultreia.java4all.bean;

/*-
 * #%L
 * Java Beans extends by Ultreia.io
 * %%
 * Copyright (C) 2018 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.bean.MyJavaBean1JavaBeanDefinition.MyJavaBean1ComparatorBuilder;
import io.ultreia.java4all.bean.MyJavaBean1JavaBeanDefinition.MyJavaBean1Predicate;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.LinkedList;
import java.util.List;

import static io.ultreia.java4all.bean.MyJavaBean1JavaBeanDefinition.MyJavaBean1Stream;

/**
 * Created by tchemit on 11/01/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class MyJavaBean1JavaBeanDefinitionTest {

    private List<MyJavaBean1> elements;

    @Before
    public void setUp() {
        elements = new LinkedList<>();
        {
            MyJavaBean1 e = new MyJavaBean1();
            e.setOne("One");
            e.setTwo("two");
            e.setThree("Three");
            e.setBigBoolean(true);
            elements.add(e);
        }
        {
            MyJavaBean1 e = new MyJavaBean1();
            e.setOne("Two");
            elements.add(e);
        }
        {
            MyJavaBean1 e = new MyJavaBean1();
            e.setOne("Z-Three");
            elements.add(e);
        }
    }


    @Test
    public void testClear() {

        new MyJavaBean1().clear();

        MyJavaBean1 myJavaBean1 = elements.get(0);
        Assert.assertNotNull(myJavaBean1.getBigBoolean());
        Assert.assertNotNull(myJavaBean1.getOne());
        Assert.assertNotNull(myJavaBean1.getTwo());
        Assert.assertNotNull(myJavaBean1.getThree());
        myJavaBean1.clear();
        Assert.assertNull(myJavaBean1.getBigBoolean());
        Assert.assertNull(myJavaBean1.getOne());
        Assert.assertNull(myJavaBean1.getTwo());
        // not cleared (since not a public property)
        Assert.assertNotNull(myJavaBean1.getThree());
    }

    @Test
    public void testComparator() {
        {
            MyJavaBean1ComparatorBuilder comparatorBuilder = MyJavaBean1JavaBeanDefinition.comparator().whereOne().sort();
            assertComparatorBuilder(comparatorBuilder, "One");
        }
        {
            MyJavaBean1ComparatorBuilder comparatorBuilder = MyJavaBean1JavaBeanDefinition.comparator().whereOne().reservedSort();
            assertComparatorBuilder(comparatorBuilder, "Z-Three");
        }
    }


    @Test
    public void testPredicate() {
        {
            MyJavaBean1Predicate predicateBuilder = MyJavaBean1JavaBeanDefinition.predicate();
            assertPredicateBuilder(predicateBuilder, true, true, false, 3);
        }
        {
            MyJavaBean1Predicate predicateBuilder = MyJavaBean1JavaBeanDefinition.predicate().whereOne().isEquals("One");
            assertPredicateBuilder(predicateBuilder, true, false, false, 1);
        }
        {
            MyJavaBean1Predicate predicateBuilder = MyJavaBean1JavaBeanDefinition.predicate().whereOne().isNotNull();
            assertPredicateBuilder(predicateBuilder, true, true, false, 3);
        }
        {
            MyJavaBean1Predicate predicateBuilder = MyJavaBean1JavaBeanDefinition.predicate().whereOne().isNullAnd().isNotNull();
            assertPredicateBuilder(predicateBuilder, false, false, true, 0);
        }
        {
            MyJavaBean1Predicate predicateBuilder = MyJavaBean1JavaBeanDefinition.predicate().whereOne().isNotEqualsAnd("One").isNotEquals("Two");
            assertPredicateBuilder(predicateBuilder, true, false, false, 1);
        }
        {
            MyJavaBean1Predicate predicateBuilder = MyJavaBean1JavaBeanDefinition.predicate().whereOne().contains("On");
            assertPredicateBuilder(predicateBuilder, true, false, false, 1);
        }
        {
            MyJavaBean1Predicate predicateBuilder = MyJavaBean1JavaBeanDefinition.predicate().whereOne().notContains("On");
            assertPredicateBuilder(predicateBuilder, true, false, false, 2);
        }

    }

    @Test
    public void testStream() {
        {
            MyJavaBean1Stream predicateBuilder = MyJavaBean1JavaBeanDefinition.stream(elements);
            assertStreamPredicate(predicateBuilder, true, true, false, 3);
        }
        {
            MyJavaBean1Stream predicateBuilder = MyJavaBean1JavaBeanDefinition.stream(elements).whereOne().isEquals("One");
            assertStreamPredicate(predicateBuilder, true, false, false, 1);
        }
        {
            MyJavaBean1Stream predicateBuilder = MyJavaBean1JavaBeanDefinition.stream(elements).whereOne().isNotNull();
            assertStreamPredicate(predicateBuilder, true, true, false, 3);
        }
        {
            MyJavaBean1Stream predicateBuilder = MyJavaBean1JavaBeanDefinition.stream(elements).whereOne().isNullAnd().isNotNull();
            assertStreamPredicate(predicateBuilder, false, false, true, 0);
        }
        {
            MyJavaBean1Stream predicateBuilder = MyJavaBean1JavaBeanDefinition.stream(elements).whereOne().isNotEqualsAnd("One").isNotEquals("Two");
            assertStreamPredicate(predicateBuilder, true, false, false, 1);
        }
        {
            MyJavaBean1Stream predicateBuilder = MyJavaBean1JavaBeanDefinition.stream(elements).whereOne().contains("On");
            assertStreamPredicate(predicateBuilder, true, false, false, 1);
        }
        {
            MyJavaBean1Stream predicateBuilder = MyJavaBean1JavaBeanDefinition.stream(elements).whereOne().notContains("On");
            assertStreamPredicate(predicateBuilder, true, false, false, 2);
        }

        {
            MyJavaBean1Stream stream = MyJavaBean1JavaBeanDefinition.stream(elements).whereOne().sort();
            assertStreamSort(stream, "One");
        }
        {
            MyJavaBean1Stream stream = MyJavaBean1JavaBeanDefinition.stream(elements).whereOne().reservedSort();
            assertStreamSort(stream, "Z-Three");
        }

    }

    private void assertPredicateBuilder(MyJavaBean1Predicate predicateBuilder, boolean expectedAnyMatch, boolean expectedAllMatch, boolean expectedNoneMatch, int expectedCount) {
        Assert.assertEquals(expectedAnyMatch, elements.stream().anyMatch(predicateBuilder));
        Assert.assertEquals(expectedAllMatch, elements.stream().allMatch(predicateBuilder));
        Assert.assertEquals(expectedNoneMatch, elements.stream().noneMatch(predicateBuilder));
        Assert.assertEquals(expectedCount, elements.stream().filter(predicateBuilder).count());
    }

    private void assertComparatorBuilder(MyJavaBean1ComparatorBuilder query, String expectedCount) {
        MyJavaBean1 myJavaBean1 = elements.stream().min(query.build().orElseThrow(NullPointerException::new)).orElseThrow(NullPointerException::new);
        Assert.assertEquals(expectedCount, myJavaBean1.getOne());
    }

    private void assertStreamPredicate(MyJavaBean1Stream stream, boolean expectedAnyMatch, boolean expectedAllMatch, boolean expectedNoneMatch, int expectedCount) {
        Assert.assertEquals(expectedAnyMatch, stream.anyMatch());
        Assert.assertEquals(expectedAllMatch, stream.allMatch());
        Assert.assertEquals(expectedNoneMatch, stream.noneMatch());
        Assert.assertEquals(expectedCount, stream.filter().count());
    }

    private void assertStreamSort(MyJavaBean1Stream stream, String expectedCount) {
        MyJavaBean1 myJavaBean1 = stream.min().orElseThrow(NullPointerException::new);
        Assert.assertEquals(expectedCount, myJavaBean1.getOne());
    }
}
